<?php

namespace app\Rf\Modules\Users;

//use app\models\Rf\Proxies;

/**
 * Class Module
 * @package app\Rf\Modules\Proxies
 *
 */
class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();

        \Yii::configure($this, require __DIR__ . DIRECTORY_SEPARATOR .
            'config' . DIRECTORY_SEPARATOR .
            'config.php');

        $this->setConsoleControllerNamespace();
    }

    protected function setConsoleControllerNamespace()
    {
        if (\Yii::$app instanceof \yii\console\Application) {
            $this->controllerNamespace = __NAMESPACE__ . '\commands';
        }
    }
}