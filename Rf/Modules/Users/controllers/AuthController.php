<?php

namespace app\Rf\Modules\Users\controllers;

use app\Rf\Modules\Users\models\auth\ResetPasswordToken;
use Yii;
use app\models\behaviors\UpdateOnlineBehavior;
use app\Rf\Modules\Users\models\auth\ForgetPassword;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class AuthController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'updateOnline' => UpdateOnlineBehavior::class,
            'access' => [
                'class' => AccessControl::class,
                'only' => ['login', 'forget-password', 'reset-password', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'forget-password', 'reset-password'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \app\Rf\Modules\Users\models\LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Forget password action.
     *
     * @return Response|string
     */
    public function actionForgetPassword()
    {
        $this->layout = '@app/views/layouts/main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgetPassword(['scenario' => ForgetPassword::SCENARIO_EMAIL]);

        if ($model->load(Yii::$app->request->post()) && $model->createResetPasswordToken()) {
            return $this->render('forget', [
                'model' => $model,
                'success_message' => "На почту {$model->email} отправлено сообщение для востановления доступа к аккаунту",
            ]);
        }

        return $this->render('forget', [
            'model' => $model,
        ]);
    }

    /**
     * Reset password action.
     *
     * @param string $token
     *
     * @throws NotFoundHttpException
     *
     * @return Response|string
     */
    public function actionResetPassword($token)
    {
        $this->layout = '@app/views/layouts/main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $tokenModel = ResetPasswordToken::find()->where(['token' => $token])->one();
        if($tokenModel == null) {
            throw new NotFoundHttpException();
        }

        $model = new ForgetPassword(['scenario' => ForgetPassword::SCENARIO_PASSWORD]);

        if ($model->load(Yii::$app->request->post()) && $model->resetPassword()) {
            Yii::$app->session->setFlash('login_success', 'Пароль успешно изменен');
            return $this->redirect(['login']);
        }

        return $this->render('reset_password', [
            'model' => $model,
            'token' => $token,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
