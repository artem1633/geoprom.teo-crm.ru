<?php

namespace app\Rf\Modules\Users\controllers;

use app\models\LoginForm;
use app\Rf\Modules\Users\models\UserAction;
use Yii;
use yii\base\Action;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;


abstract class BasePermissionController extends Controller
{
    /**
     * @return array
     */
    abstract public function userActions();

    /**
     * @param Action $action
     * @return bool
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {

        $userAction = $this->userActions()[$action->id] ?? [];
        $this->checkPermission($userAction);

        return parent::beforeAction($action);
    }

    /**
     * @param $action
     * @throws ForbiddenHttpException
     */
    protected function checkPermission($userAction)
    {
        /**
         * @var \app\Rf\Modules\Users\models\User $user
         */
        $user = Yii::$app->user->identity;

        if (!$user) {
            throw new ForbiddenHttpException('Not allowed for guest');
        }

        if (!$user->roleCan($userAction)) {
            throw new ForbiddenHttpException('Not allowed for ' . UserAction::label($userAction));
        }
    }

}
