<?php

namespace app\Rf\Modules\Users\models\auth;

use Yii;
use app\Rf\Modules\Users\models\User;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reset_password_token".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $token Токен
 * @property string $created_at
 *
 * @property User $user
 */
class ResetPasswordToken extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reset_password_token';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @param int $user_id
     * @return ResetPasswordToken
     */
    public static function generate($user_id)
    {
        $model = self::findOne(['user_id' => $user_id]);

        if($model == null)
        {
            $model = new self([
                'user_id' => $user_id,
                'token' => Yii::$app->security->generateRandomString(),
            ]);
        } else {
            $model->token = Yii::$app->security->generateRandomString();
        }

        $model->save(false);

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'token' => 'Токен',
            'created_at' => 'Создан',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
