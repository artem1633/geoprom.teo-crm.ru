<?php

namespace app\Rf\Modules\Users\models\auth;

use Yii;
use app\Rf\Modules\Users\models\User;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class ForgetPassword
 * @package app\Rf\Modules\Users\auth
 */
class ForgetPassword extends Model
{
    const SCENARIO_EMAIL = 'email';

    const SCENARIO_PASSWORD = 'password';

    /**
     * @var string Email привязанный к пользователю
     */
    public $email;

    /**
     * @var string Пароль
     */
    public $password;

    /**
     * @var string Повторо пароля
     */
    public $repeatPassword;

    /**
     * @var string Токен востановления пароля
     */
    public $resetPasswordToken;


    /**
     * @var User
     */
    private $_user;

    /**
     * @var ResetPasswordToken
     */
    private $_tokenModel;

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return ArrayHelper::merge(parent::scenarios(), [
            self::SCENARIO_EMAIL => ['email'],
            self::SCENARIO_PASSWORD => ['password', 'repeatPassword', 'resetPasswordToken'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'password', 'repeatPassword', 'resetPasswordToken'], 'required'],
            ['email', 'email'],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => \Yii::t('app', 'Wrong repeat password')],

            ['email', function(){
                $user = User::find()->where(['login' => $this->email])->one();
                $this->_user = $user;

                if($user == null){
                    $this->addError('email', 'Данный E-mail не найден');
                }
            }],

            ['resetPasswordToken', function(){
                $tokenModel = ResetPasswordToken::find()->where(['token' => $this->resetPasswordToken])->one();
                $this->_tokenModel = $tokenModel;

                if($tokenModel == null){
                    $this->addError('resetPasswordToken', 'Данного токена не существует');
                }
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль'
        ];
    }

    /**
     * Создать и отправить токен востановления пароля
     * @return bool
     */
    public function createResetPasswordToken()
    {
        if($this->validate())
        {
            $resetToken = ResetPasswordToken::generate($this->_user->id);
            $resetHref = Url::toRoute(['reset-password', 'token' => $resetToken->token], 'https');

            try {
                Yii::$app->mailer->compose()
                    ->setTo($this->_user->login)
                    ->setFrom('geoprom.notifications@yandex.ru')
                    ->setSubject('Восстановление пароля')
                    ->setHtmlBody("Ссылка для восcтановления пароля: ".Html::a($resetHref, $resetHref))
                    ->send();
            } catch (\Exception $e)
            {
                $this->addError('email', 'Во время отправки сообщения произошла ошибка');
                $resetToken->delete();
                return false;
            }


            return true;
        }

        return false;
    }

    /**
     * Изменить пароль
     * @return bool
     */
    public function resetPassword()
    {
        if($this->validate())
        {
            $user = $this->_tokenModel->user;
            $user->setPassword($this->password);
            $user->save(false);

            $this->_tokenModel->delete();
            return true;
        }

        return false;
    }
}