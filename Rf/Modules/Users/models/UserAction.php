<?php

namespace app\Rf\Modules\Users\models;

abstract class UserAction
{
    const USER_MANAGE = 'user_manage';

    const TASK_CREATE = 'TASK_CREATE';
    const TASK_UPDATE = 'TASK_UPDATE';
    const TASK_DELETE = 'TASK_DELETE';
    const TASK_VIEW = 'TASK_VIEW';

    public static function label($action)
    {
        return [
                self::USER_MANAGE => 'Users management',
                self::TASK_CREATE => 'Create task',
                self::TASK_DELETE => 'Delete task',
                self::TASK_UPDATE => 'Update task',
                self::TASK_VIEW => 'View task',
            ][$action] ?? $action;
    }

}