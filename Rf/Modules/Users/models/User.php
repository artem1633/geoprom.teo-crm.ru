<?php

namespace app\Rf\Modules\Users\models;

use app\models\notification\Notification;
use app\models\team\TeamMember;
use app\models\user\dependents\userRole\RoleDependent;
use app\models\user\dependents\userRole\UserRoleDependentFactory;
use app\models\user\Online;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $login
 * @property string $name
 * @property string $access_token
 * @property string $access_key
 * @property string $password_visible
 *
 * @property string $password_hash
 * @property string $created_at
 * @property string $updated_at
 * @property integer $role_id
 * @property Role $role
 *
 * @property bool $isAdmin
 *
 * @property RoleDependent $roleDependent
 * @property Notification[] $notifications
 *
 * @property Online $online
 * @property boolean $isOnline
 */
class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    public $changePassword;
    public $confirmPassword;

    private $password;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{rf_user}}';
    }

    /**
     * @return Role
     * @throws \ErrorException
     */
    public function getRole()
    {
        return Role::getInstance($this->role_id);
    }

    /**
     * @param Role $role
     */
    public function setRole(Role $role)
    {
        $this->role_id = $role->id;
    }

    /*public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }*/

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'login', 'role_id'/*, 'password'*/], 'required'],
            ['login', 'email'],
            [['name', 'login', 'password_visible'], 'string'],
            [['role_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['login'], 'string', 'max' => 255],
            [['login'], 'unique'],
            [['changePassword', 'password'], 'safe'],
        ];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function beforeSave($insert)
    {
        $expression = new Expression('NOW()');
        $now = (new \yii\db\Query)->select($expression)->scalar();  // SELECT NOW();
        if ($this->isNewRecord) {
            $this->generateAuthKey();
            $this->generateToken();
            $this->created_at = $now;
            $this->updated_at = $now;
        } else {
            if (!empty($this->changePassword)) {
                $this->setPassword($this->changePassword);
            }
            $this->updated_at = $now;
        }
        return parent::beforeSave($insert);
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'login' => Yii::t('users', 'Login'),
            'name' => Yii::t('users', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'role_id' => Yii::t('users', 'Role'),
            'changePassword' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by login
     *
     * @param string $login
     * @return static|null
     */
    public static function findByLogin($login)
    {
        return static::findOne(['login' => $login]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->access_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
        if ($password) {
            $this->password_visible = $password;
            $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        }
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->access_key = Yii::$app->security->generateRandomString();
    }

    public function generateToken()
    {
        $this->access_token = Yii::$app->security->generateRandomString();
    }

    public function roleCan($userActionName, $item = null)
    {
        return $this->role->can($userActionName, $this, $item);
    }

    /**
     * @return \app\models\user\dependents\userRole\RoleDependent
     * @throws \Exception
     */
    public function getRoleDependent()
    {
        return (new UserRoleDependentFactory([
        ]))->create($this);
    }

    public function getIsAdmin()
    {
        return in_array($this->getRole(), [
            Role::superAdmin(),
            Role::admin(),
        ]);
    }

    /**
     * @return array
     * id => user
     */
    public static function filterSelfList()
    {
        $list = [null => ''];

        foreach (ArrayHelper::map(self::find()
            ->select(['id', 'name'])
            ->from(self::tableName())
            ->where(['not', ['id' => Yii::$app->user->id]])
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name') as $id => $name) {
            $list[$id] = $name;
        }

        return $list;
    }


    /**
     * @return array
     * id => user
     */
    public static function filterSelfAndAdminsList()
    {
        $list = [null => ''];
        $model=self::find()
            ->select(['rf_user.id', 'name','online.seen'])
            ->from(self::tableName())
            ->leftJoin('online','rf_user.ID=online.user_id')
            ->where(['not', ['rf_user.id' => Yii::$app->user->id]])
            ->andWhere(['<', 'role_id', Role::ADMIN])
            ->orderBy('name')
            ->asArray()
            ->all();
        foreach (ArrayHelper::map($model, 'id', function($model){
                return $model['name'].' ('.$model['seen'].')';
            }) as $id => $name) {
            $list[$id] = $name;
        }

        return $list;
    }

    /**
     * @return array
     * id => user
     */
    public static function allList()
    {
        $list = [];
        foreach (ArrayHelper::map(self::find()
            ->select(['id', 'name'])
            ->from(self::tableName())
            ->orderBy('name')
            ->asArray()
            ->all(), 'id', 'name') as $id => $name) {
            $list[$id] = $name;
        }

        return $list;
    }

    /**
     * @return null|User
     */
    public static function current()
    {
        return \Yii::$app->user->identity;
    }

    public function getNotifications()
    {
        return $this->hasMany(Notification::class, ['user_id' => 'id']);
    }

    public function getOnline()
    {
        return $this->hasOne(Online::class, ['user_id' => 'id']);
    }

    public function getIsOnline()
    {
        $online = $this->online;

        if (!$online) {
            return false;
        }

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($online->seen, new \DateTimeZone(\Yii::$app->timeZone));

        return $nowDt->getTimestamp() - $seen->getTimestamp() < Yii::$app->params['onlineSeenLimit'];
    }

    /**
     * Устанавливает присутсвует ли он на странице задачи в данный момент
     * @param \app\models\task\Task $task
     * @return bool
     */
    public function getIsInTaskOnline($task)
    {
        /** @var TeamMember $member */
        $member = TeamMember::find()->where(['task' => $task->id, 'user_id' => $this->id])->one();

        if(!$member) {
            return false;
        }

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($member->task_seen, new \DateTimeZone(\Yii::$app->timeZone));

        return $nowDt->getTimestamp() - $seen->getTimestamp() < Yii::$app->params['onlineSeenLimit'];
    }
}
