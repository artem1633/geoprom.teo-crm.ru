<?php

namespace app\Rf\Modules\Users\models;

use app\models\Rf\State\BaseState;
use app\models\team\TeamMember;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\models\roles\AdminRole;
use app\Rf\Modules\Users\models\roles\AdvancedUserRole;
use app\Rf\Modules\Users\models\roles\SuperAdminRole;
use app\Rf\Modules\Users\models\roles\UserRole;
use yii\helpers\ArrayHelper;

/**
 * Class Role
 * @package app\Rf\Modules\Users\models
 *
 */
abstract class Role extends BaseState
{
    const ALLOWED_TASK_ALL = 'ALLOWED_TASK_ALL';

    const SUPER_ADMIN = 999;
    const ADMIN = 900;
    const ADVANCED_USER = 200;
    const USER = 100;
    const GUEST = 0;

    public static function list()
    {
        return [
            self::SUPER_ADMIN => 'Super Admin',
            self::ADMIN => 'Администратор',
            self::ADVANCED_USER => 'Расширенный пользователь',
            self::USER => 'Пользователь'
        ];
    }

    public static function superAdmin()
    {
        return self::getInstance(self::SUPER_ADMIN);
    }

    public static function admin()
    {
        return self::getInstance(self::ADMIN);
    }

    public static function user()
    {
        return self::getInstance(self::USER);
    }

    public static function advancedUser()
    {
        return self::getInstance(self::ADVANCED_USER);
    }

    protected static function getClass($id)
    {
        switch ($id) {
            case self::SUPER_ADMIN:
                return SuperAdminRole::class;
            case self::ADMIN:
                return AdminRole::class;
            case self::ADVANCED_USER:
                return AdvancedUserRole::class;
            case self::USER:
                return UserRole::class;


            default:
                throw new \InvalidArgumentException('Invalid id for role:' . $id);
        }
    }

    /**
     * @return array
     */
    abstract function getPermissions();


    protected function checkPermission($permission, $userActionName, $user, $item)
    {
        if (is_callable($permission)) {
            $can = call_user_func($userActionName, $user, $item);
        } else {
            $can = $permission;
        }
        return $can;
    }

    public function can($userActionName, $user = null, $item = null)
    {
        $allPermissions = $this->getPermissions();
        $permissions = $allPermissions[$userActionName] ?? false;

        if ($permissions === false) {
            return false;
        }

        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                $can = $this->checkPermission($permission, $userActionName, $user, $item);
                if ($can) {
                    break;
                }
            }
        } else {
            $can = $permissions;
        }

        return $can;
    }

    /**
     * @return []|string
     */
    public function getAllowedTaskIds()
    {
        return $this->getMemberTaskList();
    }

    /**
     * @return mixed
     */
    protected function getMemberTaskList()
    {
        $membership = TeamMember::find()
            ->select('task_id')
            ->from(TeamMember::tableName())
            ->where([
                'user_id' => User::current()->id,
            ])->asArray()->all();

        return ArrayHelper::map($membership, 'task_id', 'task_id');
    }

    /**
     * @return []
     */
    public function getMyTeamRoleFilter()
    {
        return TeamRole::memberList();
    }
}