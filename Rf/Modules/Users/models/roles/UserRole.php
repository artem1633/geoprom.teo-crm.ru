<?php

namespace app\Rf\Modules\Users\models\roles;


use app\Rf\Modules\Users\models\Role;
use app\Rf\Modules\Users\models\UserAction;

class UserRole extends Role
{
    public function getPermissions()
    {
        return [
            UserAction::USER_MANAGE => false,
            UserAction::TASK_VIEW => true,
            UserAction::TASK_DELETE => false,
            UserAction::TASK_UPDATE => false,
            UserAction::TASK_CREATE => true,
        ];
    }
}