<?php

namespace app\Rf\Modules\Users\models\roles;

use app\models\team\TeamRole;
use app\Rf\Modules\Users\models\Role;
use app\Rf\Modules\Users\models\User;
use app\Rf\Modules\Users\models\UserAction;

class AdminRole extends Role
{
    public function getPermissions()
    {
        return [
            UserAction::USER_MANAGE => function (User $user, $item) {
                return true;
            },
            UserAction::TASK_VIEW => true,
            UserAction::TASK_DELETE => true,
            UserAction::TASK_UPDATE => true,
            UserAction::TASK_CREATE => true,
        ];
    }

    public function getAllowedTaskIds()
    {
        return Role::ALLOWED_TASK_ALL;
    }

    public function getMyTeamRoleFilter()
    {
        return TeamRole::list();
    }
}