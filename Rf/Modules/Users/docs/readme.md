
config/web.php
```php
'user' => [
            'class' => 'yii\web\User',
            'identityClass' => \app\Rf\Modules\Users\models\User::class,
            'enableAutoLogin' => true,
            'loginUrl' => ['users/auth/login']
        ],
```

```php
'modules' => [
        'users' => [
            'class' => \app\Rf\Modules\Users\Module::class,
            // ... другие настройки модуля ...
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
```
config/console.php
```php
'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                //'app\migrations', // Общие миграции приложения
            ],
            //'migrationPath' => null
        ],
        'migrate-users' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\Rf\Modules\Users\migrations'
            ],
            'migrationTable' => 'rf_users_migration',
            'migrationPath' => null
        ],
    ],
    
    ....
    
    'modules' => [
            'users' => [
                'class' => \app\Rf\Modules\Users\Module::class,
                //[]
            ],
        ]
    
    
```
        `
        
        


`php yii migrate-users`