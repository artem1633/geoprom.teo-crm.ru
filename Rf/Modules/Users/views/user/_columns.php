<?php

use yii\helpers\Url;
use app\Rf\Modules\Users\models\UserAction;
use app\Rf\Modules\Users\models\User;
use app\Rf\Modules\Users\models\Role;

/**
 * @var \app\Rf\Modules\Users\models\User $identity
 */
$identity = Yii::$app->user->identity;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'login',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
        'format' => 'raw',
        'value' => function ($model) {
            return \app\widgets\user\UserInfo::widget(['user' => $model]);
        }
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'access_token',
//    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'access_key',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'role_id',
        'filter' => Role::list(),
        'value' => function (User $model) {
            return $model->role->label;
        }
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'password_hash',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'updated_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'template' => $identity->role->can(UserAction::USER_MANAGE) ?
            '{view} {update} {delete}' : '{view}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'Edit'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'Delete'),
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'],
    ],
];   