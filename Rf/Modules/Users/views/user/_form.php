<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\Rf\Modules\Users\models\Role;

/* @var $this yii\web\View */
/* @var $model app\Rf\Modules\Users\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role_id')->dropDownList(Role::list(), ['prompt' => 'Выберите роль...']) ?>

    <?php if ($model->isNewRecord): ?>
        <?= $form->field($model, 'password')->textInput() ?>
    <?php else: ?>
        <?= $form->field($model, 'changePassword')->textInput(['value' => $model->password_visible]) ?>
    <?php endif; ?>


    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ?
                Yii::t('app', 'Create') :
                Yii::t('app', 'Update'),
                ['class' => $model->isNewRecord ?
                    'btn btn-success' :
                    'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
