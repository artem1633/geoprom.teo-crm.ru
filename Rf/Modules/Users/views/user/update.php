<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Rf\Modules\Users\models\User */
?>
<div class="user-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
