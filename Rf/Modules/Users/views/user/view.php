<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Rf\Modules\Users\models\User */
?>
<div class="user-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'login',
            'name',
            //'access_token',
            //'access_key',
            //'role_id',
            [
                'label' => Yii::t('users', 'Role'),
                'value' => $model->role->label,

            ],
            //'password_hash',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
