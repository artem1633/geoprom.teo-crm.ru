<?php


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\Rf\Modules\Users\models\auth\ForgetPassword;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\Rf\Modules\Users\models\LoginForm */

$this->title = 'Восстановить пароль';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];

?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b><?= Yii::$app->name ?></b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <?php if(isset($success_message)){ ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <strong>Успех!</strong> <?=$success_message?>
            </div>
        <?php } else { ?>
            <p class="login-box-msg">Введите ваш E-mail</p>

            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

            <?= $form
                ->field($model, 'email', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

            <div class="row">
                <!-- /.col -->
                <div class="col-xs-12">
                    <?= Html::submitButton(Yii::t('app', 'Next'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
                </div>
                <!-- /.col -->
            </div>

            <?php ActiveForm::end(); ?>
        <?php } ?>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
