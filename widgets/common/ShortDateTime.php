<?php

namespace app\widgets\common;

class ShortDateTime extends \yii\bootstrap\Widget
{
    public $format = 'HH:mm dd.M.Y';

    public $dateTime;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return \Yii::$app->formatter->asDatetime($this->dateTime, $this->format);
    }
}
