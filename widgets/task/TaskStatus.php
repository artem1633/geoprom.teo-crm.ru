<?php

namespace app\widgets\task;

use app\models\task\Task;
use yii\helpers\Html;

class TaskStatus extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    public $wrapTag = 'p';

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->task->status->label;
        //return Html::tag($this->wrapTag, $this->task->status->label, ['class' => 'task-status ' . $this->task->status->labelClass]);
    }
}
