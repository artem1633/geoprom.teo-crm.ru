<?php

namespace app\widgets\task;

class DoneButton extends BaseStatusButton
{
    protected function getStatus()
    {
        return \app\models\task\TaskStatus::done();
    }

    protected function getButtonClass()
    {
        return 'btn btn-success';
    }
}
