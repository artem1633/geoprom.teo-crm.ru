<?php

namespace app\widgets\task;

use app\models\task\Task;
use yii\helpers\Html;

class AddObserverButton extends \yii\bootstrap\Widget
{
    const ACTION = 'add-observer';

    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('add-observer-button-modal', [
            'id' => 'add-observer',
            'header' => Html::tag('h2',
                \Yii::t('app', 'Add observer')),
            'body' => $this->getContent(),
            'footer' => false, //'Footer',
            'toggleButton' => [
                'label' => \Yii::t('app', 'Add observer'),
                'class' => $this->getButtonClass(),
            ],
        ]);
    }

    protected function getContent()
    {
        return $this->render('add-observer-button-modal-content', [
            'task' => $this->task,
            'buttonClass' => $this->getButtonClass(),
            'message' => '',
        ]);
    }

    protected function getButtonClass()
    {
        return 'btn btn-primary';
    }
}
