<?php

namespace app\widgets\task;

class WaitButton extends BaseStatusButton
{
    protected function getStatus()
    {
        return \app\models\task\TaskStatus::wait();
    }

    protected function getButtonClass()
    {
        return 'btn btn-danger';
    }
}
