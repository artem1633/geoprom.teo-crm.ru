<?php

namespace app\widgets\task;

class AcceptButton extends BaseStatusButton
{
    protected function getStatus()
    {
        return \app\models\task\TaskStatus::accepted();
    }

    protected function getButtonClass()
    {
        return 'btn btn-success';
    }
}
