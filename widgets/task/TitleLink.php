<?php

namespace app\widgets\task;

use app\models\task\Task;
use yii\bootstrap\Html;
use yii\helpers\Url;

class TitleLink extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Html::a($this->task->title,
            Url::toRoute(['/task-full', 'taskId' => $this->task->id]),
            ['style' => 'display: block; width: 100%; height: 100%;','data-pjax' => 0]);
    }

}
