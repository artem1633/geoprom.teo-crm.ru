<?php
/**
 * @var \app\models\task\Task $task
 */
?>

<div class="row task-stat">
    <div class="col-sm-1"><strong><?= Yii::t('app', 'Status') ?></strong>:</div>
    <div class="col-sm-3"><?= \app\widgets\task\TaskStatus::widget(['task' => $task]) ?></div>
    <div class="col-sm-4"><strong><?= Yii::t('app', 'Created At') ?></strong>: <?= $task->created_at ?></div>

    <?php if (!empty($task->finished_at)): ?>
        <div class="col-sm-4"><strong><?= Yii::t('app', 'Finished At') ?></strong>: <?= $task->finished_at ?></div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-md-12 body pre-scrollable"><?= Yii::t('app', 'Content') . ': ' .
        \yii\helpers\Html::encode($task->body) ?>
    </div>
</div>


