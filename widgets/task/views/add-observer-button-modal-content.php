<?php

use yii\bootstrap\Button;

/**
 * @var \app\models\task\Task $task
 * @var string $buttonClass
 * @var string $message
 */

$observer = \app\models\team\TeamMember::observer(null, $task);

?>
<div>
    <p>
        <?= $message ?>
    </p>
</div>
<div class="row">
    <div class="col-xs-12">

        <?php $form = \yii\bootstrap\ActiveForm::begin([
            'action' => \yii\helpers\Url::toRoute('task-full/add-observer'),
            'method' => 'post',
            'options' => [

                'data-pjax' => true,
            ]

        ]); ?>

        <?= $form->field($observer, 'task_id')->hiddenInput()->label(false) ?>
        <?= $form->field($observer, 'user_id')->dropDownList($task->getPotentialObserverList()); ?>

        <?= Button::Widget([
            'label' => \Yii::t('app', 'Confirm'),
            'options' => [
                'class' => $buttonClass,
                'onclick' => "$('.modal-backdrop').remove();$(document.body).removeClass('modal-open');"
            ],
        ]) ?>
        <?php \yii\bootstrap\ActiveForm::end(); ?>

    </div>
</div>

