<?php

use yii\bootstrap\Button;
use yii\bootstrap\Html;

/**
 * @var \app\models\task\Task $task
 * @var \app\models\task\TaskStatus $status
 * @var string $buttonClass
 * @var string $message
 */

?>
<div>
    <p>
        <?= $message ?>
    </p>
</div>
<div class="row">
    <div class="col-xs-12">
        <?= Html::beginForm(\yii\helpers\Url::toRoute('task-full/change-status'), 'post', ['data-pjax' => true]) ?>
        <?= Html::hiddenInput('taskId', $task->id) ?>
        <?= Html::hiddenInput('statusId', $status->id) ?>
        <?= Button::Widget([
            'label' => \Yii::t('app', 'Confirm'),
            'options' => [
                'class' => $buttonClass,
                'onclick' => "$('.modal-backdrop').remove();$(document.body).removeClass('modal-open');"
            ],
        ]) ?>
        <?= Html::endForm() ?>
    </div>
</div>

