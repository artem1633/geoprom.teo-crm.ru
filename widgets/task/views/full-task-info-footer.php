<?php
/**
 * @var \app\models\task\Task $task
 */
?>


<div class="row">
    <?= \app\widgets\task\StatusButtons::widget(['task' => $task]) ?>
</div>
