<?php

/**
 * @var string $header
 * @var string $body
 * @var string $footer
 * @var array $toggleButton
 * @var string $id
 */

\yii\bootstrap\Modal::begin([
    'header' => $header,
    'toggleButton' => $toggleButton,
    'footer' => $footer,
    'id' => $id,
]);
echo $body;
\yii\bootstrap\Modal::end();
