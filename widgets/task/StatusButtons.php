<?php

namespace app\widgets\task;

use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use yii\helpers\Html;

class StatusButtons extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        $content = '';

        $allRoleButtons = $this->task->status->getActionButtons();
        $roleButtons = $allRoleButtons[$this->task->getTeamRole(User::current())->id] ?? [];

        foreach ($roleButtons as $buttonClass) {
            $content .= Html::tag('div',
                $buttonClass::widget(['task' => $this->task]),
                ['class' => 'col-xs-6  col-sm-4 col-md-2']);
        }

        return $content;
    }
}
