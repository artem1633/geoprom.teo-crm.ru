<?php

namespace app\widgets\task;

use app\models\task\Task;
use app\widgets\Panel;
use yii\helpers\Html;

class FullTaskInfo extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Panel::widget([
            'header' => true, // show header or false not showing
            'headerTitle' => $this->getHeaderTitle(),
            'content' => $this->getContent(), // some content in body
            'footer' => true, // show footer or false not showing
            'footerTitle' => $this->getFooter(), // Title for footer
            'type' => 'primary', // get style for panel \amass\panel::TYPE_DEFAULT  default
        ]);
    }

    protected function getHeaderTitle()
    {
        return
            \Yii::t('app', 'Task') . ': ' .
            Html::encode($this->task->title);
    }

    protected function getContent()
    {
        return $this->render('full-task-info-body', ['task' => $this->task]);
    }

    protected function getFooter()
    {
        return $this->render('full-task-info-footer', ['task' => $this->task]);
    }

}
