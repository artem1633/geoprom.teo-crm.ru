<?php

namespace app\widgets\task;

use app\models\task\Task;
use yii\helpers\Html;

abstract class BaseStatusButton extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('status-button-modal', [
            'id' => 'set-status-' . $this->getStatus()->id,
            'header' => Html::tag('h2',
                \Yii::t('app', 'Set status') . ': ' .
                $this->getStatus()->label),
            'body' => $this->getContent(),
            'footer' => false, //'Footer',
            'toggleButton' => [
                'label' => $this->getStatus()->buttonLabel,
                'class' => $this->getButtonClass(),
            ],
        ]);
    }

    /**
     * @return \app\models\task\TaskStatus
     */
    abstract protected function getStatus();

    protected function getContent()
    {
        return $this->render('status-button-modal-content', [
            'task' => $this->task,
            'status' => $this->getStatus(),
            'buttonClass' => $this->getButtonClass(),
            'message' => 'Подтвердите установку нового статуса',

        ]);
    }

    /**
     * @return string
     */
    abstract protected function getButtonClass();

}
