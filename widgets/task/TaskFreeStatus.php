<?php

namespace app\widgets\task;

use yii\helpers\Html;

class TaskFreeStatus extends \yii\bootstrap\Widget
{
    /**
     * @var TaskStatus
     */
    public $status;

    public $wrapTag = 'span';

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Html::tag($this->wrapTag, $this->status->label, ['class' => 'task-status ' . $this->status->labelClass]);
    }
}
