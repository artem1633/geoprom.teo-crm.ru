<?php

namespace app\widgets\task;

use app\models\task\Task;
use yii\bootstrap\Collapse;

class ShortBody extends \yii\bootstrap\Widget
{
    const LIMIT = 150;
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->getBodyPart() . $this->getCollapsedFullBody();
    }

    protected function getBodyPart()
    {
        if (strlen($this->task->body) > self::LIMIT) {
            return substr($this->task->body, 0, self::LIMIT) . '...';
        } else return $this->task->body;
    }

    protected function getCollapsedFullBody()
    {
        if (strlen($this->task->body) < self::LIMIT) {
            return null;
        }

        return Collapse::widget(
            ['items' => [
                [
                    'label' => 'Полное содержание...',
                    'content' => $this->task->body,
                ]]]);
    }

}
