<?php
/**
 * @var \app\models\task\Task $task
 */
?>

<div class="row pre-scrollable">
    <?php foreach ($task->team as $user): ?>
        <div class="col-sm-12">
            <?= \app\widgets\user\TeamMemberInfo::widget([
                'user' => $user,
                'task' => $task,
            ]) ?>
        </div>
    <?php endforeach; ?>
</div>

