<?php

namespace app\widgets\task\full_task;

use app\models\task\Task;
use app\models\task\TaskStatus;
use app\Rf\Modules\Users\models\User;
use app\widgets\Panel;
use app\widgets\task\AddObserverButton;

class FullTaskTeam extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Panel::widget([
            'header' => true, // show header or false not showing
            'headerTitle' => $this->getHeaderTitle(),
            'content' => $this->getContent(), // some content in body
            'footer' => true, // show footer or false not showing
            'footerTitle' => $this->getFooter(), // Title for footer
            'type' => 'primary', // get style for panel \amass\panel::TYPE_DEFAULT  default
        ]);
    }

    protected function getHeaderTitle()
    {
        return \Yii::t('app', 'Team');
    }

    protected function getContent()
    {
        return $this->render('team-body', ['task' => $this->task]);
    }

    protected function getFooter()
    {
        //add observer can only author and if task is not accepted yet
        return
            ($this->task->status !== TaskStatus::accepted() &&
                $this->task->author->id == User::current()->id &&
                count($this->task->getPotentialObserverList()) > 1 /*1st = empty string*/
            ) ?
                AddObserverButton::widget(['task' => $this->task]) :
                null;
    }

}
