<?php

namespace app\widgets\user;

use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use yii\bootstrap\Html;

class TeamMemberInfo extends \yii\bootstrap\Widget
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var Task
     */
    public $task;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return
            Html::tag('div',
                UserInfo::widget(['user' => $this->user]) .
                \yii\bootstrap\Html::tag('div', $this->task->getTeamRole($this->user)->label,
                    ['class' => $this->task->getTeamRole($this->user)->labelClass]),
                ['class' => 'well well-sm team-member-info']
            );
    }
}
