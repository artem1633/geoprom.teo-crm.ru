<?php

namespace app\widgets\user;

use app\Rf\Modules\Users\models\User;
use app\widgets\common\ShortDateTime;
use yii\helpers\Html;

class OnlineStatus extends \yii\bootstrap\Widget
{
    const ONLINE = 'Online';
    const NO_DATA = 'No data';
    const CLASS_ONLINE = 'online-status';
    const CLASS_OFFLINE = 'offline-status';
    const DATA_ID = 'online-status';

    /**
     * @var User
     */
    public $user;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Html::tag('div', $this->getContent(), [
            'class' => $this->getClass(),
            'data-id' => self::DATA_ID,
            'user-id' => $this->user->id,
        ]);
    }

    protected function getOnlineMessage()
    {

    }

    protected function getOfflineMessage()
    {

    }

    protected function getContent()
    {
        switch ($this->user->isOnline) {
            case true:
                return Html::tag('p', \Yii::t('app', self::ONLINE),
                    ['class' => 'label label-success seen-at']
                );
            default:
                return Html::tag('p',
                    ($this->user->online ?
                        //\Yii::t('app', 'Seen at') . ': ' .
                        ShortDateTime::widget(['dateTime' => $this->user->online->seen]) :

                        \Yii::t('app', self::NO_DATA)),

                    ['class' => 'label label-warning seen-at']
                );
        }
    }

    protected function getClass()
    {
        switch ($this->user->isOnline) {
            case true:
                return self::CLASS_ONLINE;
            default:
                return self::CLASS_OFFLINE;
        }

    }
}
