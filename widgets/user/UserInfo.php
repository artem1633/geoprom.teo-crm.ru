<?php

namespace app\widgets\user;

use app\Rf\Modules\Users\models\User;
use yii\helpers\Html;

class UserInfo extends \yii\bootstrap\Widget
{
    /**
     * @var User
     */
    public $user;

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return Html::tag('div', $this->user->name .
            OnlineStatus::widget(['user' => $this->user])
            , ['class' => 'user-info']);
    }
}
