<?php
namespace app\widgets\wrapper;

class BulkButtonWidget extends \johnitvn\ajaxcrud\BulkButtonWidget
{

    public function run()
    {
        $content = '<div class="pull-left">' .
            '<span class="glyphicon glyphicon-arrow-right"></span>&nbsp;&nbsp;' . \Yii::t('app',
                'With selected') . '&nbsp;&nbsp;' .
            $this->buttons .
            '</div>';
        return $content;
    }
}

?>
