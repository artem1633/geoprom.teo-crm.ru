<?php

use app\models\message\Message;
use app\models\message\MessageType;
use app\widgets\common\ShortDateTime;
use yii\helpers\Html;
use app\models\team\TeamMember;

/**
 * @var \app\models\task\Task $task
 * @var bool $isFull
 * @var int $messagesSort
 */

\johnitvn\ajaxcrud\CrudAsset::register($this);

if(function_exists('myMessageClass') == false)
{
    function myMessageClass(Message $message)
    {
        return $message->user_id == \app\Rf\Modules\Users\models\User::current()->id ? ' my-message' : null;
    }
}

if(function_exists('serviceClass') == false)
{
    function serviceClass(Message $message)
    {
        return $message->type === MessageType::service() ? ' service' : null;
    }
}

if(function_exists('layout') == false)
{
    function layout(Message $message)
    {
        if (\app\Rf\Modules\Users\models\User::current()->id == $message->user_id)
            return 'left';

        return 'right';
    }
}

if(function_exists('encode') == false)
{
    function encode(Message $message)
    {
        if ($message->type = MessageType::service()) {
            return $message->text;
        }
        return \yii\helpers\Html::encode($message->text);
    }
}

if(function_exists('isMessageRead') == false)
{
    /**
     * @param Message $message
     * @param TeamMember[] $members
     * @return bool
     */
    function isMessageRead($message, $members)
    {
        $messageDateTime = strtotime($message->created_at);

        foreach ($members as $member)
        {
            $lastMemberOnline = strtotime($member->task_seen);
            if(($lastMemberOnline - $messageDateTime) < 0){
                return false;
                break;
            }
        }

        return true;
    }
}

if(Yii::$app->controller->id == 'task-full')
{
    $script = <<< JS
    var lastMessage = null;
    
       var chat = document.getElementById("chat-body");
        chat.scrollTop = chat.scrollHeight;
JS;
    $this->registerJs($script);
}

$taskMessages = $task->getMessagesList();

if($isFull == false)
{
    if(count($taskMessages) > 5){
        $offsetCount = count($taskMessages) - 5;
        $taskMessages = array_slice($taskMessages, $offsetCount);
    }

    if($messagesSort == SORT_DESC){
        $taskMessages = array_reverse($taskMessages);
    }
}

$lastMessageTime = count($task->messagesList) ?
    $task->messagesList[0]->created_at :
    (new DateTime('now'))->format('Y-m-d H:i:s');



?>

<?php \yii\widgets\Pjax::begin(['id' => 'chat-body', 'enablePushState' => false]); ?>

    <script>lastMessage = "<?= $lastMessageTime ?>";</script>
    <div class="row pre-scrollable">
        <div class="col-sm-12">
            <?php if (!count($task->messagesList)): ?>

                <div class="center-block alert alert-info">
                    <?= Yii::t('app', 'No messages') ?>
                </div>

            <?php else: ?>

                <?php foreach ($taskMessages as $message): ?>

                    <?php if (layout($message) == 'left') : ?>

                        <div class="row message">
                            <div class="<?=$isFull == true ? 'col-sm-3 col-md-2' : 'col-sm-12 col-md-12'?>">
                                <?= $message->user->name ?>
                                <?= \app\widgets\user\OnlineStatus::widget(['user' => $message->user]) ?>
                            </div>
                            <div class="col-sm-6 col-md-8 well well-sm<?= myMessageClass($message) ?><?= serviceClass($message) ?>">
                        <span class="small label label-default">
                            <?= ShortDateTime::widget(['dateTime' => $message->created_at]) ?>
                        </span>&nbsp;
                                <strong>
                                    <?= encode($message) ?>
                                </strong>

                                    <?php if(isMessageRead($message, $task->getTeamMembers()->andWhere(['!=', 'user_id', Yii::$app->user->getId()])->all())): ?>
                                        <span class="read-marker yes pull-right">
                                            <i class="fa fa-check"></i>
                                        </span>
                                    <?php else: ?>
                                        <span class="read-marker pull-right">
                                            <i class="fa fa-check"></i>
                                        </span>
                                    <?php endif; ?>

                                <?php $files = $message->files; ?>

                                <?php if(count($files) > 0 && $isFull): ?>
                                    <div style="margin-top: 10px;">
                                        <?php foreach ($files as $file): ?>

                                            <?php
                                                $fileName = $file->name;
                                                if(iconv_strlen($fileName, 'UTF-8') > 25){
                                                    $fileName = iconv_substr ($fileName, 0 , 25, "UTF-8" ).'...';
                                                }
                                            ?>

                                            <?php if($file->isImage): ?>
                                                <?= Html::a(Html::img($file->path, ['class' => 'chat-image']), $file->path, ['class' => 'gallery-element', 'title' => $file->name, 'data-pjax' => 0]) ?>
                                            <?php else: ?>
                                                <?= Html::a("<i class=\"fa fa-file\"></i><br><span>{$fileName}</span>", $file->path, ['class' => 'file-element', 'download' => $file->name, 'data-pjax' => 0]) ?>
                                            <?php endif; ?>

                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>

                                <?php if(Yii::$app->user->identity->getIsAdmin()): ?>
                                    <?=\yii\helpers\Html::a('<i class="fa fa-trash"></i>', ['message-delete', 'id' => $message->id], ['class' => 'pull-right text-danger btn-deleter'])?>
                                <?php endif; ?>

                            </div>

                        </div>


                    <?php else: ?>

                        <div class="col-sm-12">
                            <div class="row message">
                                <div class="<?=$isFull == true ? 'col-sm-8' : 'col-sm-12'?> col-lg-offset-2 well well-sm<?= myMessageClass($message) ?><?= serviceClass($message) ?>">
                                <span class="small label label-default">
                                    <?= ShortDateTime::widget(['dateTime' => $message->created_at]) ?>
                                </span>&nbsp;
                                    <strong>
                                        <?= encode($message) ?>
                                    </strong>

                                    <?php $files = $message->files; ?>

                                    <?php if(count($files) > 0 && $isFull): ?>
                                        <div style="margin-top: 10px;">
                                            <?php foreach ($files as $file): ?>

                                                <?php
                                                $fileName = $file->name;
                                                if(iconv_strlen($fileName, 'UTF-8') > 25){
                                                    $fileName = iconv_substr ($fileName, 0 , 25, "UTF-8" ).'...';
                                                }
                                                ?>

                                                <?php if($file->isImage): ?>
                                                    <?= Html::a(Html::img($file->path, ['class' => 'chat-image']), $file->path, ['class' => 'gallery-element', 'title' => $file->name, 'data-pjax' => 0]) ?>
                                                <?php else: ?>
                                                    <?= Html::a("<i class=\"fa fa-file\"></i><br><span>{$fileName}</span>", $file->path, ['class' => 'file-element', 'download' => $file->name, 'data-pjax' => 0]) ?>
                                                <?php endif; ?>

                                            <?php endforeach; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php if(Yii::$app->user->identity->getIsAdmin()): ?>
                                        <?=\yii\helpers\Html::a('<i class="fa fa-trash"></i>', ['message-delete', 'id' => $message->id], ['class' => 'pull-right text-danger btn-deleter'])?>
                                    <?php endif; ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= $message->user->name ?>
                                    <?= \app\widgets\user\OnlineStatus::widget(['user' => $message->user]) ?>
                                </div>

                            </div>
                        </div>

                    <?php endif; ?>


                <?php endforeach; ?>


            <?php endif; ?>


        </div>
    </div>

<?php \yii\widgets\Pjax::end(); ?>

<?php

$script = <<< JS
$('.btn-deleter').click(function(e){
    e.preventDefault();
    if(confirm('Вы действительно хотите удалить это сообщение?')){
        var url = $(this).attr('href');
    
        $.get(url).done(function(response){
            alert('Сообщение успешно удалено');
        });
    }
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>
