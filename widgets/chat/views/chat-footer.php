<?php

use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model app\models\chat\SendMessageForm */
/* @var $uploadModel \app\models\forms\UploadForm */

$fileInputWidgetOptions = [
    'language' => 'en',
    'options' => ['multiple' => true,],
    'pluginOptions' => [
        'showUpload' => false,
        'layoutTemplates' => [
            'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
        ],
        'previewFileType' => 'any',
        'overwriteInitial' => false,
        'initialPreviewAsData' => true,
        'preferIconicPreview' => true,
//            'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
        'previewFileIconSettings' => [ // configure your icon file extensions
            'docx' => \kartik\icons\Icon::show('file-word', ['class' => 'text-primary']),
            'doc' => \kartik\icons\Icon::show('file-word', ['class' => 'text-primary']),
            'xls' => \kartik\icons\Icon::show('file-excel', ['class' => 'text-success']),
            'xlsx' => \kartik\icons\Icon::show('file-excel', ['class' => 'text-success']),
        ],
        'browseLabel' => 'Выберите',
        'removeLabel' => 'Удалить',
        'dropZoneTitle' => 'Выберите и перетащите файлы'
    ],
];

$fileInputWidgetOptionsJson = json_encode($fileInputWidgetOptions['pluginOptions']);

?>

<?php $form = ActiveForm::begin([
    'id' => 'send-message-form',
    'action' => \yii\helpers\Url::toRoute('task-full/send-message'),
    'method' => 'post',
    'options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data'],
]); ?>

<?= $form->field($model, 'taskId')->hiddenInput()->label(false) ?>
<?= $form->field($model, 'userId')->hiddenInput()->label(false) ?>

<?= $form->field($model, 'text')->textarea(['rows' => 3, 'autofocus' => true])->label(false) ?>

<div class="hidden">
    <?php
//        echo $form->field($uploadModel, 'file[]')->fileInput(['multiple' => true])
    ?>
</div>

    <?= $form->field($uploadModel, 'file[]')->widget(FileInput::class, $fileInputWidgetOptions)->label('Прикрепляемые файлы') ?>

<div class="form-group">
    <?php
//        echo Html::a('<i class="fa fa-file"></i>', '#', ['id' => 'upload-files-btn', 'class' => 'btn btn-default', 'onclick' => 'event.preventDefault(); $("#uploadform-file").trigger("click");'])
    ?>
    <?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn btn-primary btn-block',
        'name' => 'send-message-button']) ?>
</div>

<?php ActiveForm::end(); ?>

<?php

$script = <<< JS
    $('#uploadform-file').change(function(){
        var count = $(this)[0].files.length;
        $('#upload-files-btn').html('<i class="fa fa-file"></i> Загружено: '+count);
    });
    $( document ).ajaxComplete(function( event, xhr, settings ) {
        $('#uploadform-file').change(function(){
            var count = $(this)[0].files.length;
            $('#upload-files-btn').html('<i class="fa fa-file"></i> Загружено: '+count);
        });
        var config = $fileInputWidgetOptionsJson;
        $('#uploadform-file').fileinput('refresh', config);
         $("#chat-body .pre-scrollable").animate({ scrollTop: $('#chat-body .pre-scrollable > .col-sm-12').height()-$('#chat-body .pre-scrollable').height() },0);
    });
    
    var dropZone = $('#sendmessageform-text');
    
    // dropZone[0].ondragover = function() {
    //     dropZone.addClass('dragable-hover');
    //     return false;
    // };
    //    
    // dropZone[0].ondragleave = function() {
    //     dropZone.removeClass('dragable-hover');
    //     return false;
    // };
    //
    // dropZone[0].ondrop = function(event) {
    //     event.preventDefault();
    //     dropZone.removeClass('hover');
    //     dropZone.addClass('drop');
    //    
    //     var file = event.dataTransfer.files[0];
    //    
    //     console.log(file);
    //    
    //     $('#uploadform-file').fileinput('addToStack', file);
    //     var stack = $('#uploadform-file').fileinput('getFileStack');
    //     $('#uploadform-file').fileinput('refresh', {initialPreview: stack});
    //     {showCaption: false}
    // };
    
    // dropZone.addEventListener('dragover', function(e) {
    //     e.stopPropagation();
    //     e.preventDefault();
    //     e.dataTransfer.dropEffect = 'copy';
    // });
    //
    // // Get file data on drop
    // dropZone.addEventListener('drop', function(e) {
    //     e.stopPropagation();
    //     e.preventDefault();
    //     var files = e.dataTransfer.files; // Array of all files
    //
    //     for (var i=0, file; file=files[i]; i++) {
    //         if (file.type.match(/image.*/)) {
    //             var reader = new FileReader();
    //
    //             reader.onload = function(e2) {
    //                 // finished reading file data.
    //                 var img = document.createElement('img');
    //                 img.src= e2.target.result;
    //                 document.body.appendChild(img);
    //             }
    //
    //             reader.readAsDataURL(file); // start reading the file data.
    //         }
    //     }
    // });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>