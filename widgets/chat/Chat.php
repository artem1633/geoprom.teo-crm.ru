<?php

namespace app\widgets\chat;

use Yii;
use app\assets\plugins\MagnificPopup;
use app\models\chat\SendMessageForm;
use app\models\forms\UploadForm;
use app\models\task\Task;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\models\User;
use app\widgets\Panel;

/**
 * Class Chat
 * @package app\widgets\chat
 */
class Chat extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * @var SendMessageForm
     */
    public $sendMessageForm = null;

    /**
     * @var bool
     */
    private $isFull = true;

    /**
     * @var int
     */
    public $messagesSort = SORT_ASC;

    /**
     * @return string
     */
    public function getSmall()
    {
        MagnificPopup::register(Yii::$app->view);
        $this->messagesSort = SORT_DESC;

        if (is_null($this->sendMessageForm)) {
            $this->sendMessageForm = new SendMessageForm();
            $this->sendMessageForm->taskId = $this->task->id;
            $this->sendMessageForm->userId = User::current()->id;
        }

        $this->isFull = false;

        return $this->getContent();
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        MagnificPopup::register(Yii::$app->view);

        if (is_null($this->sendMessageForm)) {
            $this->sendMessageForm = new SendMessageForm();
            $this->sendMessageForm->taskId = $this->task->id;
            $this->sendMessageForm->userId = User::current()->id;
        }

        $this->isFull = true;

        return Panel::widget([
            'header' => true, // show header or false not showing
            'headerTitle' => $this->getHeaderTitle(),
            'content' => $this->getContent(), // some content in body
            'footer' => true, // show footer or false not showing
            'footerTitle' => $this->getFooter(), // Title for footer
            'type' => 'primary', // get style for panel \amass\panel::TYPE_DEFAULT  default
        ]);
    }

    protected function getHeaderTitle()
    {
        return \Yii::t('app', 'Chat');
    }

    protected function getContent()
    {
        return $this->render('chat-body', ['task' => $this->task, 'isFull' => $this->isFull, 'messagesSort' => $this->messagesSort]);
    }

    protected function getFooter()
    {
        $uploadModel = new UploadForm();
        return
            $this->task->getTeamRole(User::current()) !== TeamRole::foreign() ?
                $this->render('chat-footer', ['model' => $this->sendMessageForm, 'uploadModel' => $uploadModel]) :
                null;
    }
}
