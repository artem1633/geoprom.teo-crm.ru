<?php

namespace app\widgets\columns;

use kartik\grid\DataColumn as BaseDataColumn;
use yii\helpers\Html;

/**
 * Class DataColumn
 * @package app\widgets\columns
 */
class DataColumn extends BaseDataColumn
{
    protected function renderFilterCellContent()
    {
        $input = Html::activeTextInput($this->grid->filterModel, $this->attribute, $this->filterInputOptions);

        return '<div style="position: relative;">'.$input.'<i class="fa fa-times" style="position: absolute; top: 10px; right: 8px;
        color: #cecece; cursor: pointer;
        " onclick="$(this).parent().find(\'input\').val(\'\'); $(\'#'.$this->grid->id.'\').yiiGridView(\'applyFilter\');"></i></div>';
    }
}
