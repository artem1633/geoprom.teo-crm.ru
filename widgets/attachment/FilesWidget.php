<?php

namespace app\widgets\attachment;

use app\models\attachment\AddAttachmentsForm;
use app\models\task\Task;

class FilesWidget extends \yii\bootstrap\Widget
{
    /**
     * @var Task
     */
    public $task;

    /**
     * @var AddAttachmentsForm
     *
     */
    public $filesForm = null;

    /**
     * Брать ли файлы из сообщений в чате задачи
     * @var bool
     */
    public $ignoreMessages = true;


    /**
     * {@inheritdoc}
     */
    public function run()
    {
        if (is_null($this->filesForm)) {
            $this->filesForm = new AddAttachmentsForm();
            $this->filesForm->taskId = $this->task->id;
        }

        $initialPreview = [];
        $previewConfig = [];
        foreach ($this->task->attachments as $attachment) {
            $initialPreview[] = $attachment->srcUrl;
            $previewConfig[] = $attachment->getPreviewConfig();
        };


        return $this->render('files', [
            'task' => $this->task,
            'filesForm' => $this->filesForm,
            'initialPreview' => $initialPreview,
            'previewConfig' => $previewConfig,
            'ignoreMessages' => $this->ignoreMessages,
        ]);

    }
}
