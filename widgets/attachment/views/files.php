<?php

use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use app\models\message\Message;

/**
 * @var \app\models\task\Task $task
 * @var \app\models\attachment\AddAttachmentsForm $filesForm
 * @var string[] $initialPreview
 * @var array $previewConfig
 * @var boolean $ignoreMessages
 */

if($ignoreMessages == false){
    $messages = Message::find()->where(['task_id' => $task->id])->with('files')->all();

    $messagesPreview = [];

    foreach ($messages as $message) {
        $messagesPreview = \yii\helpers\ArrayHelper::merge($messagesPreview, \yii\helpers\ArrayHelper::getColumn($message->getFiles()->all(), 'path'));
    }

    $initialPreview = \yii\helpers\ArrayHelper::merge($messagesPreview, $initialPreview);
}


?>
<?php \yii\widgets\Pjax::begin(['id' => 'files', 'enablePushState' => false]); ?>

<?php $form = ActiveForm::begin([
    'id' => 'attachment-form',
    'action' => \yii\helpers\Url::toRoute('/attachment/add'),
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data',
        'data-pjax' => true
    ],

]); ?>

<?= $form->field($filesForm, 'files[]')->widget(FileInput::class, [
    'options' => ['multiple' => true,],
    'pluginOptions' => [
        'fileActionSettings' => [
            'showRemove' => \app\Rf\Modules\Users\models\User::current()->isAdmin,
        ],
        'layoutTemplates' => [
            'actionDownload' => '<a class="{downloadClass}" href="{downloadUrl}" target="_blank" data-pjax="0" title="Загрузить" download="{caption}"><i class="glyphicon glyphicon-download"></i></a> '
        ],
        'initialPreview' => $initialPreview,
        'previewFileType' => 'any',
        'overwriteInitial' => false,
        'initialPreviewAsData' => true,
        'preferIconicPreview' => true,
        'initialPreviewConfig' => $previewConfig,
        'initialPreviewDownloadUrl' => \yii\helpers\Url::to('uploads/' . $task->id . '/{filename}', true),
        'previewFileIconSettings' => [ // configure your icon file extensions
            'docx' => \kartik\icons\Icon::show('file-word', ['class' => 'text-primary']),
            'doc' => \kartik\icons\Icon::show('file-word', ['class' => 'text-primary']),
            'xls' => \kartik\icons\Icon::show('file-excel', ['class' => 'text-success']),
            'xlsx' => \kartik\icons\Icon::show('file-excel', ['class' => 'text-success']),
        ]

    ],
])->label(false); ?>

<?= $form->field($filesForm, 'taskId')->hiddenInput()->label(false) ?>

<?php ActiveForm::end(); ?>

<?php \yii\widgets\Pjax::end(); ?>
