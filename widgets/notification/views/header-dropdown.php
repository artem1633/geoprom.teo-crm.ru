<?php

use yii\helpers\Url;

/**
 * @var int $count
 */

$user = \app\Rf\Modules\Users\models\User::current();
$notifications = $user->notifications;
$count = count($notifications);


?>

<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
        <i class="fa fa-bell-o"></i>
        <?php if ($count > 0): ?>
            <span class="label label-warning"><?= $count ?></span>
        <?php endif; ?>
    </a>
    <li>
    <a href="<?=Url::toRoute(['/settings/index'])?>">
        <i class="fa fa-cog"></i>
    </a>
</li>
    <ul class="dropdown-menu">
        <?php if ($count == 0): ?>
            <li class="header">
                <?= \Yii::t('app', 'You not have notifications') ?>
            </li>
        <?php else: ?>
            <li class="header">
                <?= \Yii::t('app', 'You have {count} notifications', [
                    'count' => $count,
                ]) ?>
            </li>

            <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                    <?php foreach ($notifications as $notification): ?>

                        <li>
                            <?= \yii\bootstrap\Html::a('<i class="' . $notification->event->iconClass . '"></i>' .
                                $notification->event->label,
                                \yii\helpers\Url::toRoute(['/task-full', 'taskId' => $notification->task_id]),
                                ['data-pjax' => 0, 'title' => Yii::t('app', Yii::t('app', 'View')),]) ?>
                            <p class="well well-sm text-sm">
                                <?= $notification->message ?>
                            </p>
                        </li>

                    <?php endforeach; ?>

                    <!--                    <li>-->
                    <!--                        <a href="#">-->
                    <!--                            <i class="fa fa-users text-aqua"></i> 5 new members joined today-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="#">-->
                    <!--                            <i class="fa fa-warning text-yellow"></i> Very long description here-->
                    <!--                            that may-->
                    <!--                            not fit into the-->
                    <!--                            page and may cause design problems-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="#">-->
                    <!--                            <i class="fa fa-users text-red"></i> 5 new members joined-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="#">-->
                    <!--                            <i class="fa fa-shopping-cart text-green"></i> 25 sales made-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                    <!--                    <li>-->
                    <!--                        <a href="#">-->
                    <!--                            <i class="fa fa-user text-red"></i> You changed your username-->
                    <!--                        </a>-->
                    <!--                    </li>-->
                </ul>
            </li>

        <?php endif; ?>


        <li class="footer">
            <?= \yii\bootstrap\Html::a(Yii::t('app', 'Notifications'),
                \yii\helpers\Url::toRoute('/notification/index'),
                ['data-pjax' => 0, 'title' => Yii::t('app', 'Full view'),]) ?>
        </li>
    </ul>
</li>
