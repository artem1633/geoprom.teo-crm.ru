<?php

namespace app\widgets\notification;

use app\models\notification\Notification;
use app\models\task\TaskEvent;
use app\models\team\TeamMember;
use app\widgets\task\TaskStatus;
use app\widgets\task\TitleLink;
use yii\bootstrap\Widget;

class NotificationMessage extends Widget
{
    /**
     * @var Notification
     */
    public $notification;

    public $params = [];


    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->getMessage();
    }

    /**
     *
     */
    protected function getMessage()
    {
        switch ($this->notification->event) {
            case TaskEvent::createTask():

                return 'Новый запрос: ' .
                    TitleLink::widget(['task' => $this->notification->task]);


            case TaskEvent::observerAdd():

                /**
                 * @var TeamMember $observer
                 */
                $observer = $this->params['observer'];

                if ($this->notification->user_id == $observer->user_id) {
                    $text = 'Вас добавили как наблюдателя в запрос ';
                } else {
                    $text = 'Добавлен наблюдатель в запрос ';
                }

                return $text .
                    TitleLink::widget(['task' => $this->notification->task]);


            case TaskEvent::changeStatus():

                //if executor and set status wait
                if ($this->notification->task->executor->id == $this->notification->user->id &&
                    $this->notification->task->status === \app\models\task\TaskStatus::wait()) {
                    return 'Запрос возвращен на доработку: ' .
                        TitleLink::widget(['task' => $this->notification->task]);
                }

                //default
                return 'Изменен статус запроса: ' .
                    TitleLink::widget(['task' => $this->notification->task]) .
                    ' ' .
                    'Новый статус: ' .
                    TaskStatus::widget([
                        'task' => $this->notification->task,
                        'wrapTag' => 'span',
                    ]);

            case TaskEvent::chatMessage():

                return 'Новое сообщение: ' .
                    TitleLink::widget(['task' => $this->notification->task]);

            case TaskEvent::attachmentAdd():

                return 'Новый файл: ' .
                    TitleLink::widget(['task' => $this->notification->task]);


            default:
                throw new \InvalidArgumentException('Event not handled: ' . $this->notification->event->label);
        }
    }
}
