<?php

namespace app\widgets\notification;

use yii\bootstrap\Widget;

class HeaderDropdown extends Widget
{
    public $user;


    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->render('header-dropdown');
    }
}
