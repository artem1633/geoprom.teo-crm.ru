<?php

namespace app\models\task;

use app\models\attachment\Attachment;
use app\models\message\Message;
use app\models\message\MessageType;
use app\models\notification\Notification;
use app\models\team\TeamMember;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\models\User;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $title
 * @property string $body
 * @property int $status_id
 * @property TaskStatus $status
 *
 * @property integer $created_at
 * @property integer $finished_at
 * @property integer $executionTime Время выполнения;
 * @property TeamMember[] $teamMembers
 * @property User $author
 * @property User $executor
 * @property User[] $observers
 * @property User[] $team
 * @property Message[] $messages
 * @property Message[] $messagesList
 *
 * @property TeamRole $myTeamRole
 * @property int $teamRoleId
 *
 * @property-read [] $potentialObserverList
 *
 * @property-read Notification[] $notifications
 * @property-read Notification[] $myNotifications
 * @property-read int $myNotificationCount
 *
 * @property-read Attachment[] $attachments
 *
 */
class Task extends \yii\db\ActiveRecord
{

    public $currentUser = null;

    public $observersList;

    public function getCurrentUser()
    {
        if (is_null($this->currentUser)) {
            return User::current();
        }
        return $this->currentUser;

    }

    public function delete()
    {
        try {
            \Yii::$app->db->transaction(function ($db) {
                TeamMember::deleteAll(['task_id' => $this->id]);
                Notification::deleteAll(['task_id' => $this->id]);
                Message::deleteAll(['task_id' => $this->id]);

                //delete attachments models
                Attachment::deleteAll(['task_id' => $this->id]);

                parent::delete();
            });
        } catch (\Throwable $exception) {
            return false;
        }

        //delete attachments directory with files (if success delete models)
        FileHelper::unlink(Attachment::basePath() . $this->id);
        return true;
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        try {
            \Yii::$app->db->transaction(function ($db) use ($runValidation, $attributeNames) {

                //set finished time for first accept status save
                if ($this->status === TaskStatus::accepted()
                    && (($this->oldAttributes['status_id'] ?? null) != TaskStatus::accepted()->id)
                    && is_null($this->finished_at)) {
                    $this->finished_at = new Expression('NOW()');
                }

                //remove finished time if status is not accepted
                if ($this->status !== TaskStatus::accepted()) {
                    $this->finished_at = null;
                }

                $new = $this->isNewRecord;
                parent::save($runValidation, $attributeNames);

                //add author team member for new record
                if ($new) {
                    TeamMember::author($this->getCurrentUser(), $this)->save();
                }
            });
        } catch (\Throwable $exception) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'body', 'status_id'], 'required'],
            [['status_id', 'teamRoleId'], 'integer'],
            [['created_at', 'finished_at', 'observersList'], 'safe'],
            [['title', 'body'], 'string', 'max' => 2048],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'title' => \Yii::t('app', 'Title'),
            'body' => \Yii::t('app', 'Body'),
            'status_id' => \Yii::t('app', 'Status'),
            'status' => \Yii::t('app', 'Status'),
            'created_at' => \Yii::t('app', 'Created At'),
            'finished_at' => \Yii::t('app', 'Finished At'),
            'author' => \Yii::t('app', 'Author'),
            'executor' => \Yii::t('app', 'Executor'),
            'authorId' => \Yii::t('app', 'Author'),
            'executorId' => \Yii::t('app', 'Executor'),
            'myTeamRoleId' => \Yii::t('app', 'My Team Role'),
            'messagesList' => \Yii::t('app', 'Messages'),
            'observersList' => \Yii::t('app','Observers'),
        ];
    }

    /**
     * @return TaskStatus
     * @throws \ErrorException
     */
    public function getStatus()
    {
        return TaskStatus::getInstance($this->status_id);
    }

    /**
     * Возвращает время выполнения задачи
     * @return int
     */
    public function getExecutionTime()
    {
        $from = strtotime($this->created_at);
        $to = strtotime($this->finished_at != null ? $this->finished_at : date('Y-m-d H:i:s'));

        $start = date('Y-m-d', $from);
        $end = date('Y-m-d', $to);
        $count = 0;
        while($start != $end){
            if($start != 'st'){
                $count++;
            }

            if($count > 10){
                break;
            }
        }

//        $difference = $to - $from;
//
//        $hourDifference = floor($difference / 3600); // полная разница в минутах
//        $roundedHourDifference = $hourDifference % 3600; // c точностью до часа


        return $count;
    }

    /**
     * @param TaskStatus $role
     */
    public function setStatus(TaskStatus $status)
    {
        $this->status_id = $status->id;
    }

    /**
     * @return ActiveQuery
     */
    public function getExecutor()
    {
        return $this->getUser(TeamRole::executor());
    }

    /**
     * @param User $executor
     * @return User|null
     * @throws \Throwable
     */
    public function setExecutor(User $executor)
    {
        /**
         * @var TeamMember $member
         */
        $member = $this->getMember(TeamRole::executor())->one();

        if ($member->user->id != $executor->id) {

            \Yii::$app->db->transaction(function ($db) use ($member, $executor) {
                TeamMember::executor($executor, $this)->save();
                $member->delete();
            });
        }
    }

    public function getUser(TeamRole $role)
    {
        return $this->hasOne(User::class, ['id' => 'user_id'])
            ->viaTable(TeamMember::tableName(), ['task_id' => 'id'], function (ActiveQuery $query) use ($role) {
                $query->andFilterWhere(['team_role_id' => $role->id]);
            });
    }

    /**
     * @return User[]
     */
    public function getTeam()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(TeamMember::tableName(), ['task_id' => 'id']);
    }

    public function getUsers(TeamRole $role)
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(TeamMember::tableName(), ['task_id' => 'id'], function (ActiveQuery $query) use ($role) {
                $query->andFilterWhere(['team_role_id' => $role->id]);
            });
    }

    public function getAuthor()
    {
        return $this->getUser(TeamRole::author());
    }

    /**
     * @param TeamRole $teamRole
     * @return \yii\db\ActiveQuery
     */
    protected function getMember(TeamRole $teamRole)
    {
        return $this->getTeamMembers()
            ->with('user')
            ->andFilterWhere(['team_role_id' => $teamRole->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamMembers()
    {
        return $this->hasMany(TeamMember::class, ['task_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getObservers()
    {
        return $this->getUser(TeamRole::observer());
    }

    public function isAuthor(User $user)
    {
        return $this->author->id == $user->id;
    }

    /**
     * @param User $user
     * @return TeamRole
     */
    public function getTeamRole(User $user)
    {
        foreach ($this->teamMembers as $member) {
            if ($member->user->id == $user->id) {
                return $member->role;
            }
        }
        return TeamRole::foreign();
    }

    public function isExecutor(User $user)
    {
        return $this->executor->id == $user->id;
    }


    public function getMeIsAuthor()
    {
        return $this->author->id == \Yii::$app->user->id;
    }

    public function getMeIsExecutor()
    {
        return $this->executor->id == \Yii::$app->user->id;
    }

    /**
     * @return TeamRole
     */
    public function getMyTeamRole()
    {
        return $this->getTeamRole(User::current());
    }

    /**
     * @return int
     */
    public function getTeamRoleId()
    {
        return $this->getMyTeamRole()->id;
    }

    /**
     * @return array
     */
    public function getPotentialObserverList()
    {
        $all = User::allList();
        $team = $this->team;
        $selected = [null => ''];

        foreach ($all as $userId => $name) {
            $inTeam = false;
            foreach ($team as $user) {
                if ($userId == $user->id) {
                    $inTeam = true;
                    break;
                }
            }
            if (!$inTeam) {
                $selected[$userId] = $name;
            }
        }
        return $selected;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::class, ['task_id' => 'id']);
    }

    /**
     * @return int|string
     */
    public function getMyNotificationCount()
    {
        return $this->getNotifications()
            ->andWhere(['user_id' => User::current()->id])
            ->count();
    }

    /**
     * @param null $count
     * @return Notification[]
     */
    public function getMyNotifications($count = null)
    {
        $q = $this->getNotifications()
            ->andWhere(['user_id' => User::current()->id])
            ->orderBy(['created_at' => SORT_DESC]);

        if (!is_null($count)) {
            $q->limit($count);
        }
        return $q->all();
    }

    /**
     * @param int
     * @return Message[]
     */
    public function getMessagesList($sort = SORT_ASC)
    {
        return $this->getMessages()
            ->andWhere(['!=', 'type_id', MessageType::SERVICE])
            ->orderBy(['created_at' => $sort])
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::class, ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttachments()
    {
        return $this->hasMany(Attachment::class, ['task_id' => 'id']);
    }
}
