<?php

namespace app\models\task;

use app\models\Rf\State\BaseState;
use app\models\task\statuses\AcceptedTaskStatus;
use app\models\task\statuses\DoneTaskStatus;
use app\models\task\statuses\SeenTaskStatus;
use app\models\task\statuses\WaitTaskStatus;
use app\models\task\statuses\BackTaskStatus;

/**
 * Class TaskStatus
 * @package app\models\task
 *
 * @property $buttonsWidget
 * @property $buttonLabel
 * @property $labelClass
 */
abstract class TaskStatus extends BaseState
{
    const WAIT = 1; //ожидает не промотрено исполнителем

    const SEEN = 2; // просмотрен исполнителем

    const BACK = 3; // возврат инициатору

    const DONE = 5; // исполнитель отметил как завершенное

    const ACCEPTED = 10; //принято инициатором

    public static function list()
    {
        return [
            self::WAIT => 'Ожидание',
            self::SEEN => 'Просмотрено',
            self::BACK => 'Возврат',
            self::DONE => 'Выполнено',
            self::ACCEPTED => 'Завершено',
        ];
    }

    public static function buttonLabels()
    {
        return [
            self::WAIT => 'Вернуть',
            self::DONE => 'Выполнено',
            self::BACK => 'Возврат',
            self::ACCEPTED => 'Завершено',
        ];
    }

    public static function createOptionsList()
    {
        return [
            self::WAIT => 'Ожидание',
        ];
    }

    public static function wait()
    {
        return self::getInstance(self::WAIT);
    }

    public static function accepted()
    {
        return self::getInstance(self::ACCEPTED);
    }

    public static function done()
    {
        return self::getInstance(self::DONE);
    }

    public static function seen()
    {
        return self::getInstance(self::SEEN);
    }
    public static function back()
    {
        return self::getInstance(self::BACK);
    }

    protected static function getClass($id)
    {
        switch ($id) {
            case self::ACCEPTED:
                return AcceptedTaskStatus::class;
            case self::WAIT:
                return WaitTaskStatus::class;
            case self::SEEN:
                return SeenTaskStatus::class;
            case self::BACK:
                return BackTaskStatus::class;
            case self::DONE:
                return DoneTaskStatus::class;

            default:
                throw new \InvalidArgumentException('Invalid id for task status:' . $id);
        }
    }

    /**
     * @return array
     */
    abstract public function getActionButtons();

    /**
     * @return string|null
     */
    public function getButtonLabel()
    {
        return self::buttonLabels()[$this->id] ?? null;
    }

    abstract public function getLabelClass();
}