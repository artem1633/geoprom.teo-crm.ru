<?php

namespace app\models\task;

use app\models\message\Message;
use app\models\message\MessageType;
use app\models\team\roles\AuthorTeamRole;
use app\models\team\TeamMember;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\models\Role;
use app\Rf\Modules\Users\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * TaskSearch represents the model behind the search form about `app\models\task\Task`.
 */
class TaskSearch extends Task
{
    const FLAG_NEW_MESSAGES = 'new_messages';
    const FLAG_NEW_TASKS = 'new_tasks';

    public $authorId;
    public $executorId;
    public $myTeamRoleId;

    public $flags;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id'], 'integer'],
            [['authorId', 'executorId', 'myTeamRoleId', 'teamRoleId'], 'integer'],
            [['title', 'body', 'created_at', 'finished_at', 'flags'], 'safe'],
        ];
    }

    protected function getCommon($array1, $array2)
    {
        $common = [];
        foreach ($array1 as $i => $value) {
            if (in_array($value, $array2)) {
                $common[] = $value;
            }
        }
        return $common;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }


    protected function getTaskIds($allowedTask, $connection, $needleUserId)
    {
        $query = Task::find()
            ->joinWith($connection)
            ->where([User::tableName() . '.id' => $needleUserId]);

        if ($allowedTask !== Role::ALLOWED_TASK_ALL) {
            $query->andWhere(['in', Task::tableName() . '.id', $allowedTask]);
        }
        $taskIds = $query->asArray()->all();

        return ArrayHelper::map($taskIds, 'id', 'id');
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->flagEnabled(self::FLAG_NEW_TASKS)) {
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'title' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                ],
                'body' => [
                    'asc' => ['title' => SORT_ASC],
                    'desc' => ['title' => SORT_DESC],
                ],
                'myTeamRoleId' => [
                    'asc' => [TeamMember::tableName() . '.team_role_id' => SORT_ASC],
                    'desc' => [TeamMember::tableName() . '.team_role_id' => SORT_DESC],
                ],
                'authorId' => [
                    'asc' => [User::tableName() . '.name' => SORT_ASC],
                    'desc' => [User::tableName() . '.name' => SORT_DESC],
                ],
                'executorId' => [
                    'asc' => [User::tableName() . '.name' => SORT_ASC],
                    'desc' => [User::tableName() . '.name' => SORT_DESC],
                ],
                'created_at' => [
                    'asc' => ['created_at' => SORT_ASC],
                    'desc' => ['created_at' => SORT_DESC],
                ],
                'finished_at' => [
                    'asc' => ['finished_at' => SORT_ASC],
                    'desc' => ['finished_at' => SORT_DESC],
                ],
                'status_id' => [
                    'asc' => ['status_id' => SORT_ASC],
                    'desc' => ['status_id' => SORT_DESC],
                ],
            ],
            'defaultOrder' => ['status_id' => SORT_DESC, 'created_at' => SORT_DESC],
        ]);


        $this->load($params);

        if (isset($params['sort'])) {
            if ($params['sort'] == 'authorId' || $params['sort'] == '-authorId') {
                $query->joinWith('author');
            }
            if ($params['sort'] == 'executorId' || $params['sort'] == '-executorId') {
                $query->joinWith('executor');
            }
            if ($params['sort'] == 'myTeamRoleId' || $params['sort'] == '-myTeamRoleId') {
                $query->innerJoinWith('team');
//                $query->andFilterWhere(['and', [TeamMember::tableName().'.user_id' => $this->authorId], [TeamMember::tableName().'.team_role_id' => AuthorTeamRole::AUTHOR]]);
            }
        }

//        $query->innerJoinWith('team');
//        $query->andFilterWhere(['and', [TeamMember::tableName().'.user_id' => $this->authorId], [TeamMember::tableName().'.team_role_id' => AuthorTeamRole::AUTHOR]]);

        $allowedTask = User::current()->role->getAllowedTaskIds();

        $idsE = $this->executorId ?
            $this->getTaskIds($allowedTask, 'executor', $this->executorId) :
            null;

        $idsA = $this->authorId ?
            $this->getTaskIds($allowedTask, 'author', $this->authorId) :
            null;

        if ($this->authorId || $this->executorId) {

            if (is_null($idsA) && is_null($idsE)) {
                //$query->filterWhere([Task::tableName() . '.id' => 0]);
            } else {

                if (!is_null($idsA) && is_null($idsE)) {
                    $allowedTask = $idsA;

                }
                if (is_null($idsA) && !is_null($idsE)) {
                    $allowedTask = $idsE;
                }

                if (!is_null($idsA) && !is_null($idsE)) {


                    $allowedTask = $this->getCommon($idsA, $idsE);
                }
            }
        }

        if (!empty($this->myTeamRoleId)) {

            $idsTasksWithRole = [];

            if ($this->myTeamRoleId == TeamRole::foreign()->id) {
                $q = Task::find()
                    ->with('teamMembers')
                    ->all();

                /**
                 * @var Task $task
                 */
                foreach ($q as $task) {
                    $isMember = false;
                    foreach ($task->teamMembers as $teamMember) {
                        if (User::current()->id == $teamMember->user_id) {
                            $isMember = true;
                            break;
                        }
                        if ($isMember) {
                            break;
                        }
                    }
                    if (!$isMember) {
                        $idsTasksWithRole[$task->id] = $task->id;
                    }
                }
            } else {
                /**
                 * @var TeamMember[] $memberIn
                 */
                $membershipQ = TeamMember::find()
                    //->with('task')
                    //->with('user')
                    ->where([
                        'user_id' => User::current()->id,

                        //'team_role_id' => $this->myTeamRoleId,
                    ]);

                if ($allowedTask !== Role::ALLOWED_TASK_ALL) {
                    $membershipQ->andWhere(
                        ['in', 'task_id', $allowedTask]
                    );
                }


                /**
                 * @var TeamMember[] $memberIn
                 */
                $membership = $membershipQ->all();
                /**
                 * @var TeamMember $member
                 */
                foreach ($membership as $member) {

                    if ($member->role === TeamRole::getInstance($this->myTeamRoleId)) {
                        $idsTasksWithRole[$member->task_id] = $member->task_id;
                    }
                }

                if (is_array($allowedTask)) {
                    $allowedTask = $this->getCommon($allowedTask, $idsTasksWithRole);
                } else {
                    $allowedTask = $idsTasksWithRole;
                }
            }


            //var_dump($idsTasksWithRole); die;

            if (count($idsTasksWithRole) > 0) {
                if ($allowedTask !== Role::ALLOWED_TASK_ALL) {
                    $query->andWhere(['in', Task::tableName() . '.id', $allowedTask]);
                }
                $query->filterWhere(['in', Task::tableName() . '.id', $idsTasksWithRole]);
            } else {
                $query->filterWhere([Task::tableName() . '.id' => 0]);
            }
            //$query->filterWhere(['in', Task::tableName() . '.id', $common]);
        } else if($this->flagEnabled(self::FLAG_NEW_TASKS) == false) {
            $idsTasksWithRole = [];

            $q = Task::find()
                ->with('teamMembers')
                ->all();

            /**
             * @var Task $task
             */
            foreach ($q as $task) {
                $isMember = false;
                $taskMember = null;
                foreach ($task->teamMembers as $teamMember) {
                    if (User::current()->id == $teamMember->user_id) {
                        $isMember = true;
                        $taskMember = $teamMember;
                        break;
                    }
                    if ($isMember) {
                        break;
                    }
                }
                if (!$isMember) {
                    $idsTasksWithRole[$task->id] = $task->id;
                } else if ($this->flagEnabled(self::FLAG_NEW_MESSAGES)){
                    if($this->hasTaskUnreadMessages($task, $taskMember) == false)
                    {
                        $idsTasksWithRole[$task->id] = $task->id;
                    }
                }


            }

            if (count($idsTasksWithRole) > 0) {
                if ($allowedTask !== Role::ALLOWED_TASK_ALL) {
                    $query->andWhere(['not in', Task::tableName() . '.id', $allowedTask]);
                }
                $query->filterWhere(['not in', Task::tableName() . '.id', $idsTasksWithRole]);
            } else {
                $query->filterWhere([Task::tableName() . '.id' => 0]);
            }
        }


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        if ($allowedTask !== Role::ALLOWED_TASK_ALL) {
            $query->andWhere(['in', Task::tableName() . '.id', $allowedTask]);
        }


        //$query->andFilterWhere(['role_id' => $this->myTeamRoleId]);



        $query->andFilterWhere([
            'id' => $this->id,
            'status_id' => $this->status_id,
            ]);

        if($this->created_at != null)
        {
            $query->andFilterWhere(['between', 'created_at', explode(' - ', $this->created_at)[0], explode(' - ', $this->created_at)[1]]);
        }

        if($this->finished_at != null)
        {
            $query->andFilterWhere(['between', 'finished_at', explode(' - ', $this->finished_at)[0], explode(' - ', $this->finished_at)[1]]);
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'body', $this->body]);


        return $dataProvider;
    }

    public function flagEnabled($flag)
    {
        $flags = explode(',', $this->flags);
        return in_array($flag, $flags);
    }

    /**
     * @param Task $task
     * @param TeamMember $member
     * @return bool
     */
    private function hasTaskUnreadMessages($task, $member)
    {
        /** @var Message $lastMessage */
        $lastMessage = Message::find()->where(['task_id' => $task->id, 'type_id' => MessageType::REGULAR])->orderBy('created_at DESC')->one();

        if(!$lastMessage) {
            return false;
        }

        $lastMessageDateTime = strtotime($lastMessage->created_at);
        $lastOnline = strtotime($member->task_seen);

        return $lastOnline - $lastMessageDateTime < 0;
    }
}
