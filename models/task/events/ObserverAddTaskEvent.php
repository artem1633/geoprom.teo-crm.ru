<?php

namespace app\models\task\events;

use app\models\message\Message;
use app\models\message\MessageType;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\Rf\Modules\Users\models\User;

class ObserverAddTaskEvent extends TaskEvent
{
    public function getIconClass()
    {
        return 'fa fa-user-plus text-green';
    }

    /**
     * @param Task $task
     * @param User $initiator
     * @param array $params
     * @return Message|false
     * @throws \Exception
     */
    public function getServiceMessage(Task $task, User $initiator, $params = [])
    {
        return new Message([
            'type' => MessageType::service(),
            'task' => $task,
            'user' => $initiator,
            'text' => 'Добавлен наблюдатель',
        ]);
    }

}