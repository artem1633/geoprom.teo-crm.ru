<?php

namespace app\models\task\events;

use app\models\message\Message;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\Rf\Modules\Users\models\User;

class ChatMessageTaskEvent extends TaskEvent
{
    public function getIconClass()
    {
        return 'fa fa-warning text-aqua';
    }

    /**
     * @param Task $task
     * @param User $initiator
     * @param array $params
     * @return Message|false
     * @throws \Exception
     */
    public function getServiceMessage(Task $task, User $initiator, $params = [])
    {
        return false;
    }
}