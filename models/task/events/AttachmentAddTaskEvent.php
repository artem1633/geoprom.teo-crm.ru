<?php

namespace app\models\task\events;

use app\models\attachment\Attachment;
use app\models\message\Message;
use app\models\message\MessageType;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\Rf\Modules\Users\models\User;
use yii\bootstrap\Html;

class AttachmentAddTaskEvent extends TaskEvent
{
    public function getIconClass()
    {
        return 'fa fa-edit text-green';
    }

    /**
     * @param Task $task
     * @param User $initiator
     * @param array $params
     * @return Message|false
     * @throws \Exception
     */
    public function getServiceMessage(Task $task, User $initiator, $params = [])
    {
        return new Message([
            'type' => MessageType::service(),
            'task' => $task,
            'user' => $initiator,
            'text' => $this->getMessageText($task, $initiator, $params),
        ]);
    }

    /**
     * @param Task $task
     * @param User $user
     * @param array $params
     * @return string
     * @throws \Exception
     */
    protected function getMessageText(Task $task, User $user, $params = [])
    {
        /**
         * @var Attachment $attachment ;
         */
        $attachment = $params['attachment'];

        return 'Добавлен файл: ' . Html::a($attachment->file, $attachment->srcUrl, [
                'target' => '_blank', 'data-pjax' => 0
            ]);
    }
}