<?php

namespace app\models\task;

use app\models\message\Message;
use app\models\notification\Notification;
use app\models\Rf\State\BaseState;
use app\models\task\events\AttachmentAddTaskEvent;
use app\models\task\events\ChangeStatusTaskEvent;
use app\models\task\events\ChatMessageTaskEvent;
use app\models\task\events\CreateTaskEvent;
use app\models\task\events\ObserverAddTaskEvent;
use app\Rf\Modules\Users\models\User;
use app\widgets\notification\NotificationMessage;
use Yii;

/**
 * Class TaskEvent
 * @package app\models\task
 *
 * @property-read string $iconClass
 *
 */
abstract class TaskEvent extends BaseState
{
    const CREATE = 1;

    const CHANGE_STATUS = 2;

    const OBSERVER_ADD = 3;

    const CHAT_MESSAGE = 4;

    const ATTACHMENT_ADD = 5;

    public static function list()
    {
        return [
            self::CREATE => 'Создано',
            self::CHANGE_STATUS => 'Изменен статус',
            self::OBSERVER_ADD => 'Добавлен наблюдатель',
            self::CHAT_MESSAGE => 'Новое сообщение',
            self::ATTACHMENT_ADD => 'Новые файлы',
        ];
    }

    public static function createTask()
    {
        return self::getInstance(self::CREATE);
    }

    public static function chatMessage()
    {
        return self::getInstance(self::CHAT_MESSAGE);
    }

    public static function observerAdd()
    {
        return self::getInstance(self::OBSERVER_ADD);
    }

    public static function attachmentAdd()
    {
        return self::getInstance(self::ATTACHMENT_ADD);
    }

    public static function changeStatus()
    {
        return self::getInstance(self::CHANGE_STATUS);
    }

    protected static function getClass($id)
    {
        switch ($id) {
            case self::CREATE:
                return CreateTaskEvent::class;
            case self::CHANGE_STATUS:
                return ChangeStatusTaskEvent::class;
            case self::OBSERVER_ADD:
                return ObserverAddTaskEvent::class;
            case self::CHAT_MESSAGE:
                return ChatMessageTaskEvent::class;
            case self::ATTACHMENT_ADD:
                return AttachmentAddTaskEvent::class;

            default:
                throw new \InvalidArgumentException('Invalid id for task event:' . $id);
        }
    }


    /**
     * @param Task $task
     * @param User $user
     * @param array $params
     * @return Notification
     */
    public function getNotification(Task $task, User $recipient, $params = [])
    {
        $notification = new Notification([
            'event' => $this,
            'task' => $task,
            'important' => true,
            'user' => $recipient
        ]);

        $notification->message = NotificationMessage::widget([
            'notification' => $notification,
            'params' => $params,
        ]);

        return $notification;
    }

    /**
     * @param Task $task
     * @param User $initiator
     * @param array $params
     * @return Message|false
     * @throws \Exception
     */
    abstract public function getServiceMessage(Task $task, User $initiator, $params = []);


    /**
     * @param Task $task
     * @param User $eventInitiator
     * @param array $params
     * @throws \Exception
     */
    public function trigger(Task $task, User $eventInitiator, $params = [])
    {
        //notify team
        foreach ($task->teamMembers as $member) {
            //not self notify
            if ($member->user->id != $eventInitiator->id) {

                //remove previous similar notification
                Notification::deleteAll([
                    'task_id' => $task->id,
                    'user_id' => $member->user->id,
                    'event_id' => $this->id,
                ]);

                //create and save notification
                $this->getNotification($task, $member->user, $params)
                    ->save();
            }
        }

        //add service message to task chat
        $message = $this->getServiceMessage($task, $eventInitiator, $params);
        if ($message) {
            $message->save();
        }
    }

    abstract public function getIconClass();
}