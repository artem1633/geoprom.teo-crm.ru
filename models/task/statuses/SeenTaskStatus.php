<?php

namespace app\models\task\statuses;

use app\models\task\TaskStatus;
use app\models\team\TeamRole;
use app\widgets\task\AcceptButton;
use app\widgets\task\DoneButton;

class SeenTaskStatus extends TaskStatus
{
    public function getActionButtons()
    {
        return
            [
                TeamRole::foreign()->id => [],
                TeamRole::author()->id => [
                    TaskStatus::ACCEPTED => AcceptButton::class,
                ],
                TeamRole::executor()->id => [
                    TaskStatus::DONE => DoneButton::class,
                ],
                TeamRole::observer()->id => [],
            ];
    }

    public function getLabelClass()
    {
        return 'label label-warning';
    }
}