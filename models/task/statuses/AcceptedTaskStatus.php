<?php

namespace app\models\task\statuses;

use app\models\task\TaskStatus;
use app\models\team\TeamRole;

class AcceptedTaskStatus extends TaskStatus
{
    public function getActionButtons()
    {
        return
            [
                TeamRole::foreign()->id => [],
                TeamRole::author()->id => [],
                TeamRole::executor()->id => [],
                TeamRole::observer()->id => [],
            ];
    }

    public function getLabelClass()
    {
        return 'label label-success';
    }
}