<?php

namespace app\models\task\statuses;

use app\models\task\TaskStatus;
use app\models\team\TeamRole;
use app\widgets\task\AcceptButton;
use app\widgets\task\WaitButton;

class DoneTaskStatus extends TaskStatus
{
    /**
     * @return array
     * @throws \Exception
     */
    public function getActionButtons()
    {
        return
            [
                TeamRole::foreign()->id => [],
                TeamRole::author()->id => [
                    TaskStatus::ACCEPTED => AcceptButton::class,
                    TaskStatus::WAIT => WaitButton::class,
                ],
                TeamRole::executor()->id => [],
                TeamRole::observer()->id => [],
            ];

    }

    public function getLabelClass()
    {
        return 'label label-primary';
    }

}