<?php

namespace app\models\message;

use app\models\message\types\RegularMessageType;
use app\models\message\types\ServiceMessageType;
use app\models\Rf\State\BaseState;

/**
 * Class MessageType
 * @package app\models\message
 */
abstract class MessageType extends BaseState
{
    const REGULAR = 1;

    const SERVICE = 2;

    public static function list()
    {
        return [
            self::REGULAR => 'Обычное',
            self::SERVICE => 'Сервисное',
        ];
    }

    public static function regular()
    {
        return self::getInstance(self::REGULAR);
    }

    public static function service()
    {
        return self::getInstance(self::SERVICE);
    }

    protected static function getClass($id)
    {
        switch ($id) {
            case self::REGULAR:
                return RegularMessageType::class;
            case self::SERVICE:
                return ServiceMessageType::class;

            default:
                throw new \InvalidArgumentException('Invalid id for message type:' . $id);
        }
    }
}