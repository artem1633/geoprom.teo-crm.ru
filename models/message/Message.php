<?php

namespace app\models\message;

use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use Yii;

/**
 * This is the model class for table "message".
 *
 * @property int $id
 * @property string $text
 * @property int $type_id
 * @property int $task_id
 * @property int $user_id
 * @property string $created_at
 *
 * @property Task $task
 * @property User $user
 * @property MessageType $type
 * @property MessageFile[] $files
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message';
    }

    public static function create(Task $task, User $from, MessageType $type)
    {
        return new self([
            'user' => $from,
            'task' => $task,
            'type' => $type,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_id', 'task_id', 'user_id'], 'required'],
            [['type_id', 'task_id', 'user_id'], 'integer'],
            [['created_at',], 'safe'],
            [['text'], 'string', 'max' => 1024],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'text' => Yii::t('app', 'Message text'),
            'files' => Yii::t('app', 'Files'),
            'type_id' => Yii::t('app', 'Type ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(MessageFile::class, ['message_id' => 'id']);
    }

    public function setTask(Task $task)
    {
        $this->task_id = $task->id;
    }

    public function setUser(User $user)
    {
        $this->user_id = $user->id;
    }

    public function getType()
    {
        return MessageType::getInstance($this->type_id);
    }

    public function setType(MessageType $type)
    {
        $this->type_id = $type->id;
    }

    public function withText($text)
    {
        $this->text = $text;
        return $this;
    }
}
