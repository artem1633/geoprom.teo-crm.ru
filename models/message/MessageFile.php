<?php

namespace app\models\message;

use app\models\base\FileActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message_file".
 *
 * @property int $id
 * @property int $message_id Сообщение
 * @property string $name Наименование
 * @property string $path Путь
 *
 * @property Message $message
 */
class MessageFile extends FileActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'message_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['message_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['message_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'id' => 'ID',
            'message_id' => 'Сообщение',
            'name' => 'Наименование',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::class, ['id' => 'message_id']);
    }
}
