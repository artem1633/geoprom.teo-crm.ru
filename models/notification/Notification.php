<?php

namespace app\models\notification;

use app\models\task\Task;
use app\models\task\TaskEvent;
use app\Rf\Modules\Users\models\User;
use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property boolean $important
 * @property string $message
 * @property int $task_id
 * @property int $event_id
 *
 * @property TaskEvent $event
 * @property int $user_id
 * @property string $created_at
 *
 * @property Task $task
 * @property User $user
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['important', 'task_id', 'event_id', 'user_id'], 'integer'],
            [['task_id', 'user_id'], 'required'],
            [['created_at'], 'safe'],
            [['message'], 'string', 'max' => 1024],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'important' => Yii::t('app', 'Important'),
            'message' => Yii::t('app', 'Notification message'),
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'event_id' => Yii::t('app', 'Event'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    public function setTask(Task $task)
    {
        $this->task_id = $task->id;
    }

    public function setUser(User $user)
    {
        $this->user_id = $user->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getEvent()
    {
        return TaskEvent::getInstance($this->event_id);
    }

    public function setEvent(TaskEvent $event)
    {
        return $this->event_id = $event->id;
    }
}
