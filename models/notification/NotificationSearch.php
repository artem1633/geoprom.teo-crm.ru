<?php

namespace app\models\notification;

use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NotificationSearch represents the model behind the search form about `app\models\notification\Notification`.
 */
class NotificationSearch extends Notification
{
    public $taskTitle;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'event_id', 'task_id', 'user_id'], 'integer'],
            [['taskTitle', 'important', 'message', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Notification::find()
            ->joinWith('task')
            ->where(['user_id' => User::current()->id]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'message',
                'event_id',
                'created_at',
                'taskTitle' => [
                    'asc' => [Task::tableName() . '.title' => SORT_ASC],
                    'desc' => [Task::tableName() . '.title' => SORT_DESC],
                ],

            ],
            'defaultOrder' => ['created_at' => SORT_DESC],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'event_id' => $this->event_id,
            'task_id' => $this->task_id,
            'user_id' => $this->user_id,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'important', $this->important])
            ->andFilterWhere(['like', 'message', $this->message]);

        $query->andFilterWhere(['like', Task::tableName() . '.title', $this->taskTitle]);

        $sort = $params['sort'] ?? null;
        if ($sort !== 'created_at' || $sort == '-created_at') {
            $query->orderBy(['created_at' => SORT_DESC]);
        }

        return $dataProvider;
    }
}
