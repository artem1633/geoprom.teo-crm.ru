<?php

namespace app\models\user\dependents\userRole;

use app\models\Rf\Dependent\BaseDependent;

/**
 * Class RoleDependent
 * @package app\models\dependents\userRole
 *
 * @property array $menuItems
 */
abstract class RoleDependent extends BaseDependent
{
    abstract public function getMenuItems();
}