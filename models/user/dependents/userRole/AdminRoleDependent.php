<?php

namespace app\models\user\dependents\userRole;

class AdminRoleDependent extends RoleDependent
{
    public function getMenuItems()
    {
        return [
            [
                'label' =>
                    \Yii::t('users', 'Users'),
                'icon' => 'users',
                'url' => ['/users/user']
            ],
            [
                'label' =>
                    \Yii::t('app', 'Tasks'),
                'icon' => 'tasks',
                'url' => ['/task/index']
            ],
            [
                'label' => 'Завершенные задачи',
                'icon' => 'tasks',
                'url' => ['/completed-task/index']
            ],
            [
                'label' =>
                    \Yii::t('app', 'Notifications'),
                'icon' => 'bell',
                'url' => ['/notification/index']
            ],
            [
                'label' =>
                    \Yii::t('app', 'Manual'),
                'icon' => 'info-circle',
                'url' => ['/manual/index']
            ],
        ];
    }
}