<?php

namespace app\models\user\dependents\userRole;


use app\models\Rf\Dependent\BaseDependentFactory;
use app\Rf\Modules\Users\models\Role;
use app\Rf\Modules\Users\models\User;

class UserRoleDependentFactory extends BaseDependentFactory
{
    /**
     * @param User $factor
     * @return RoleDependent
     */
    public function create($factor)
    {
        switch ($factor->role) {
            case Role::superAdmin():
            case Role::admin():
                return new AdminRoleDependent();

            case Role::advancedUser():
            case Role::user():
                return new UserRoleDependent();


            default:
                throw new \Exception('Invalid factor user dependent ' . $factor->role->id);
        }
    }
}