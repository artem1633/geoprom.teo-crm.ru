<?php

namespace app\models\user;

use app\Rf\Modules\Users\models\User;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "online".
 *
 * @property int $id
 * @property string $seen
 * @property int $user_id
 *
 * @property User $user
 */
class Online extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['seen'], 'safe'],
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'seen' => Yii::t('app', 'Seen'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return $this
     */
    public function touch()
    {
        if (!$this->user_id) {
            $this->user_id = User::current()->id;
        }
        $this->seen = new Expression('NOW()');
        return $this;
    }
}
