<?php
/**
 * Created by PhpStorm.
 * User: raid3r81
 * Date: 17.10.18
 * Time: 12:15
 */

namespace app\models\behaviors;


use app\models\team\TeamMember;
use app\models\user\Online;
use app\Rf\Modules\Users\models\User;
use yii\base\Behavior;
use yii\web\Controller;

class UpdateOnlineBehavior extends Behavior
{
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => 'setUserOnline',
        ];
    }

    public function setUserOnline($event)
    {
        $user = User::current();
        if (!$user) {
            return;
        }

        $online = Online::findOne(['user_id' => $user->id]);
        if (!$online) {
            $online = new Online();
        }

        if($event->sender->id == 'task-full' && ($event->sender->action->id == 'index' || $event->sender->action->id == 'new-messages'))
        {
            $taskId = $_GET['taskId'];
            /** @var TeamMember $member */
            $member = TeamMember::find()->where(['task_id' => $taskId, 'user_id' => $user->id])->one();

            if($member != null)
            {
                $member->task_seen = date('Y-m-d H:i:s');
                $member->save(false);
            }
        }

        $online->touch()->save();
    }
}