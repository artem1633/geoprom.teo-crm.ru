<?php

namespace app\models\attachment;

use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use Yii;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "attachment".
 *
 * @property int $id
 * @property int $task_id
 * @property int $user_id
 * @property string $file
 * @property string $created_at
 *
 * @property Task $task
 * @property User $user
 *
 * @property-read string|null $filename
 * @property-read string $srcUrl
 */
class Attachment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attachment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_id', 'user_id', 'file'], 'required'],
            [['task_id', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['file'], 'string', 'max' => 256],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::class, 'targetAttribute' => ['task_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'task_id' => Yii::t('app', 'Task ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'file' => Yii::t('app', 'File'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function prepareDirectory()
    {
        if (!file_exists($this->getPath())) {
            FileHelper::createDirectory($this->getPath(), 0777, true);
        }
        if (!file_exists($this->getPath() . DIRECTORY_SEPARATOR . 'index.html')) {
            file_put_contents($this->getPath() . DIRECTORY_SEPARATOR . 'index.html', '');
        }
    }

    public static function basePath()
    {
        return \Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR;
    }

    protected function getPath()
    {
        return self::basePath() .
            $this->task_id . DIRECTORY_SEPARATOR;
    }

    public function getFilename()
    {
        $filename = $this->getPath() . $this->file;
        return $filename;
    }

    public function getPreviewConfig()
    {
        return [
            'type' => $this->getType(),
            'size' => file_exists($this->filename) ? filesize($this->filename) : 0,
            'caption' => $this->file,
            'url' => \yii\helpers\Url::toRoute(['attachment/delete', 'attachmentId' => $this->id])
            //$this->getSrcUrl(),

        ];
    }

    protected function getType()
    {
        $parts = (explode(".", $this->file));
        $ext = end($parts);

        switch ($ext) {
            case 'pdf':
                return 'pdf';

            case 'jpeg':
            case 'jpg':
            case 'png':
            case 'gif':
                return 'image';

            case 'txt':
                return 'text';

            case 'doc':
            case 'docx':
            case 'xls':
            case 'xlsx':
            case 'ppt':
                return 'office';

            case 'mp4':
                return 'video';

            case 'html':
                return 'html';

            default:
                return 'text';
        }
    }

    public function getSrcUrl()
    {
        return Url::to(\Yii::getAlias('@uploads') . DIRECTORY_SEPARATOR .
            $this->task_id . DIRECTORY_SEPARATOR . $this->file, true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    public function setTask(Task $task)
    {
        $this->task_id = $task->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function setUser(User $user)
    {
        $this->user_id = $user->id;
    }

    public function delete()
    {
        FileHelper::unlink($this->filename);
        return parent::delete();
    }
}
