<?php

namespace app\models\attachment;

use app\Rf\Modules\Users\models\User;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class AddAttachmentsForm
 * @package app\models\attachment
 *
 * @property-read Attachment[] $added
 */
class AddAttachmentsForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $files;

    public $taskId;
    /**
     * @var Attachment[]
     */
    protected $_added = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['taskId',], 'required'],

            [['files',], 'file', 'skipOnEmpty' => false, 'maxFiles' => 5],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'files' => 'Файлы',
        ];
    }

    /**
     * @return Attachment[]
     */
    public function getAdded()
    {
        return $this->_added;
    }

    public function save()
    {
        foreach ($this->files as $file) {

            $attachment = Attachment::find()
                ->where([
                    'file' => $file->name,
                    'task_id' => $this->taskId,
                ])
                ->limit(1)
                ->one();

            if (!$attachment) {
                $attachment = new Attachment();
                $attachment->file = $file->name;
                $attachment->task_id = $this->taskId;
                $attachment->user = User::current();
                $attachment->prepareDirectory();

                if (!file_exists($attachment->filename)) {
                    $file->saveAs($attachment->filename);
                    $attachment->save();

                    $this->_added[] = $attachment;

                } else {
                    $this->addError('files', "Файл {$file->name} уже загружен");
                }
            }
        }
    }
}
