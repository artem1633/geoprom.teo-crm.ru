<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class UploadForm
 * @package app\models\form
 */
class UploadForm extends Model
{
    const MAX_FILES = 10;

    /**
     * @var UploadedFile
     */
    public $file;

    /**
     * @var string
     */
    public $uploadPath = 'uploads/';

    /**
     * @var string[]
     */
    public $fullUploadedFilePath;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'maxFiles' => self::MAX_FILES],
        ];
    }

    /**
     * Загружает файлы
     * @return bool
     */
    public function upload()
    {
        if(file_exists ('uploads') == false) {
            mkdir('uploads');
        }

        if(file_exists ('uploads/chat') ==  false) {
            mkdir('uploads/chat');
        }

        if ($this->validate()) {
//            if(is_array($this->file) == false) // Если файлы есть
//            {
                foreach($this->file as $oneFile)
                {
                    $fileName = Yii::$app->security->generateRandomString(32);
                    $fullUploadedPath = $this->uploadPath . $fileName . '.' . $oneFile->extension;
                    $this->fullUploadedFilePath[] = [
                        'path' => $fullUploadedPath,
                        'name' => $oneFile->baseName.'.'.$oneFile->extension,
                    ];
                    $oneFile->saveAs($fullUploadedPath);
                }
                return true;
//            } else {
//                echo '456';
//                return false;
//            }
        } else {
            return false;
        }
    }
}