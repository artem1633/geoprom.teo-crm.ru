<?php

namespace app\models\team;

use app\models\Rf\State\BaseState;
use app\models\team\roles\AuthorTeamRole;
use app\models\team\roles\ExecutorTeamRole;
use app\models\team\roles\ForeignTeamRole;
use app\models\team\roles\ObserverTeamRole;

/**
 * Class TeamRole
 * @package app\models\team
 *
 * @property-read string $labelClass
 */
abstract class TeamRole extends BaseState
{
    const FOREIGN = -1;
    const AUTHOR = 1;
    const EXECUTOR = 2;
    const OBSERVER = 3;

    public static function list()
    {
        return [
            self::FOREIGN => 'Не участник',
            self::AUTHOR => 'Автор',
            self::EXECUTOR => 'Исполнитель',
            self::OBSERVER => 'Наблюдатель',
        ];
    }

    public static function memberList()
    {
        $list = self::list();

        unset($list[self::FOREIGN]);

        return $list;
    }

    public static function foreign()
    {
        return self::getInstance(self::FOREIGN);
    }

    public static function author()
    {
        return self::getInstance(self::AUTHOR);
    }

    public static function executor()
    {
        return self::getInstance(self::EXECUTOR);
    }

    public static function observer()
    {
        return self::getInstance(self::OBSERVER);
    }

    protected static function getClass($id)
    {
        switch ($id) {
            case self::FOREIGN:
                return ForeignTeamRole::class;
            case self::AUTHOR:
                return AuthorTeamRole::class;
            case self::EXECUTOR:
                return ExecutorTeamRole::class;
            case self::OBSERVER:
                return ObserverTeamRole::class;

            default:
                throw new \InvalidArgumentException('Invalid id for team role:' . $id);
        }
    }

    abstract public function getLabelClass();

}