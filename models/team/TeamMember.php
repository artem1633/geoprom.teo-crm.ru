<?php

namespace app\models\team;

use app\models\message\Message;
use app\models\message\MessageType;
use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "task_team_member".
 *
 * @property int $id
 * @property int $task_id
 * @property Task $task
 * @property int $user_id
 * @property User $user
 * @property int $team_role_id
 * @property string $task_seen
 * @property TeamRole $role
 *
 * @property bool $hasUnreadMessages
 */
class TeamMember extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_team_member';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['task_seen'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @param IdentityInterface $user
     * @param Task $task
     * @return TeamMember
     */
    public static function executor($user, Task $task)
    {
        return self::member($user, $task, TeamRole::executor());
    }

    /**
     * @param IdentityInterface $user
     * @param Task $task
     * @return TeamMember
     */
    public static function author($user, Task $task)
    {
        return self::member($user, $task, TeamRole::author());
    }

    /**
     * @param IdentityInterface $user
     * @param Task $task
     * @return TeamMember
     */
    public static function observer($user = null, Task $task)
    {
        return self::member($user, $task, TeamRole::observer());
    }

    /**
     * @param IdentityInterface $user
     * @param Task $task
     * @param TeamRole $role
     * @return TeamMember
     */
    protected static function member($user, Task $task, TeamRole $role)
    {
        $member = new self();
        if (!is_null($user)) {
            $member->user = $user;
        }
        $member->role = $role;
        $member->task = $task;
        return $member;
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['task_id', 'user_id', 'team_role_id'], 'required'],
            [['task_id', 'user_id', 'team_role_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'user_id' => \Yii::t('app', 'User'),
            'team_role_id' => 'Team Role ID',
            'task_seen' => \Yii::t('app', 'Last seen')
        ];
    }

    /**
     * @return TeamRole
     * @throws \ErrorException
     */
    public function getRole()
    {
        return TeamRole::getInstance($this->team_role_id);
    }

    /**
     * Есть ли непрочитанные сообщения
     * @return bool
     */
    public function getHasUnreadMessages()
    {
        /** @var Message $lastMessage */
        $lastMessage = Message::find()->where(['task_id' => $this->task_id, 'type_id' => MessageType::REGULAR])->orderBy('created_at DESC')->one();

        if(!$lastMessage) {
            return false;
        }

        $lastMessageDateTime = strtotime($lastMessage->created_at);
        $lastOnline = strtotime($this->task_seen);

        return $lastOnline - $lastMessageDateTime < 0;
    }

    /**
     * @param TeamRole $role
     */
    public function setRole(TeamRole $role)
    {
        $this->team_role_id = $role->id;
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function setUser(User $user)
    {
        $this->user_id = $user->id;
    }

    public function getTask()
    {
        return $this->hasOne(Task::class, ['id' => 'task_id']);
    }

    public function setTask(Task $task)
    {
        $this->task_id = $task->id;
    }
}
