<?php

namespace app\models\team\roles;

use app\models\team\TeamRole;

class ExecutorTeamRole extends TeamRole
{
    public function getLabelClass()
    {
        return 'label label-success';
    }
}