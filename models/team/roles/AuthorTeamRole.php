<?php

namespace app\models\team\roles;

use app\models\team\TeamRole;

class AuthorTeamRole extends TeamRole
{
    public function getLabelClass()
    {
        return 'label label-primary';
    }
}