<?php

namespace app\models\team\roles;

use app\models\team\TeamRole;

class ObserverTeamRole extends TeamRole
{
    public function getLabelClass()
    {
        return 'label label-default';
    }
}