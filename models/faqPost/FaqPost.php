<?php

namespace app\models\faqPost;

use Yii;
use \app\models\faqCategory\FaqCategory;

/**
 * This is the model class for table "faq_post".
 *
 * @property int $id
 * @property int $category_id Категория FAQ-поста
 * @property string $name Наименование поста
 * @property string $content Содержимое поста
 * @property string $created_at Создано
 * @property string $updated_at Изменено
 *
 * @property FaqCategory $category
 */
class FaqPost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faq_post';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'category_id', 'content'], 'required'],
            [['category_id'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => FaqCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Категория',
            'name' => 'Наименование',
            'content' => 'Контент',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(FaqCategory::className(), ['id' => 'category_id']);
    }
}
