<?php

namespace app\models\Rf\Builder;

use yii\base\Model;

abstract class BaseBuilder extends Model implements BuilderInterface
{
    protected $_result = null;
    protected $data = [];

    /**
     * @param array $config
     * @return $this
     */
    public static function create($config = [])
    {
        $class = get_called_class();
        return new $class($config);
    }

    /**
     * @param $required
     * @return $this
     */
    public function build($required)
    {
        $this->data = $required;
        return $this;
    }


    protected function getResult()
    {
        $this->setResult(
            call_user_func_array([$this, 'buildResult'], [$this->data])
        );
        return $this->_result;
    }

    protected function setResult($value)
    {
        $this->_result = $value;
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    abstract protected function buildResult($data);
}