<?php

namespace app\models\Rf\Builder;

/**
 * Interface BuilderInterface
 * @package app\models\Rf\Builder
 *
 * @var $result
 */
interface BuilderInterface
{
    /**
     * @param array $config
     * @return $this
     */
    public static function create($config = []);

    /**
     * @param $required
     * @return $this
     */
    public function build($required);
}