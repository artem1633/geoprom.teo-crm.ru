<?php

namespace app\models\Rf\Builder;

/**
 * Class TestBuilder
 * @package app\models\Rf\Builder
 *
 * @property-read int $result
 */
class TestBuilder extends BaseBuilder
{
    protected function buildResult($data)
    {
        return $data;
    }
}