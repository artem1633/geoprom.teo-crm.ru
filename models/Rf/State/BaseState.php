<?php

namespace app\models\Rf\State;

/**
 * Class BaseState
 * @package app\models\Rf\State
 *
 * @property-read string $id
 * @property-read string $label
 */
abstract class BaseState
{
    protected static $pool = [];

    protected $id;

    protected function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @param $id
     * @return $this
     */
    public static function getInstance($id)
    {
        $class = get_called_class();
        if (!isset(static::$pool[$class][$id])) {
            self::initPrototypes();
        }
        try {
            return static::$pool[$class][$id];
        } catch (\Throwable $exception) {
            throw new \ErrorException("Prototype not exist for subclass of {$class} with id: {$id}");
        }

    }

    protected static function initPrototypes()
    {
        $classCalled = get_called_class();
        foreach (static::list() as $id => $label) {
            $class = static::getClass($id);
            static::$pool[$classCalled][$id] = new $class($id);
        }
    }

    /**
     * [id => 'State Label',]
     *
     * @return array
     */
    abstract public static function list();

    /**
     * @param $id
     * @return string
     * @throws \InvalidArgumentException
     */
    abstract protected static function getClass($id);

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getLabel()
    {
        return self::label($this->id);
    }

    public static function label($id)
    {
        return static::list()[$id] ?? null;
    }

    public function __get($name)
    {
        $getter = 'get' . ucfirst($name);
        if (method_exists($this, $getter)) {
            return call_user_func([$this, $getter]);
        }
        throw new \RuntimeException('No getter for name:', $name);
    }
}