<?php

namespace app\models\Rf\Dependent;

use app\models\Rf\Factory\FactoryInterface;
use yii\base\Model;

/**
 * Class BaseDependentFactory
 * @package app\models\Rf\Dependent
 */
abstract class BaseDependentFactory extends Model implements FactoryInterface
{
    abstract public function create($factor);
}