<?php

namespace app\models\Rf\Adapter;

/**
 * Class ModelAttributeAdapter
 * @package app\models\Rf\Adapter
 *
 * @version 1.0.0
 */
abstract class BaseModelAttributeAdapter implements ModelAttributeAdapterInterface
{
    protected $rawItem;

    /**
     * Adapter constructor.
     * @param mixed $rawItem
     */
    function __construct($rawItem)
    {
        $this->rawItem = $rawItem;
    }

    /**
     * @return []
     */
    abstract public function getAttributes();

    /**
     * @param $value
     * @return boolean
     */
    protected function asBoolean($value)
    {
        return boolval($value);
    }

    /**
     * @param mixed $value
     * @param mixed|null $zero
     * @return int|null
     */
    protected function asInteger($value, $zero = null)
    {
        $formattedValue = intval($value);
        if ($formattedValue == 0) {
            $formattedValue = $zero;
        }
        return $formattedValue;
    }

    /**
     * @param $value
     * @param mixed|null $zero
     * @return float|null
     */
    protected function asFloat($value, $zero = null)
    {
        $formattedValue = floatval(str_replace(',', '.', $value));
        if ($formattedValue == 0) {
            $formattedValue = $zero;
        }
        return $formattedValue;
    }

    /**
     * @param $value
     * @param null $length
     * @param bool $trim
     * @param mixed|null $empty
     * @return null|string
     */
    protected function asText($value, $length = null, $trim = true, $empty = null)
    {
        $value = $trim ? trim($value) : $value;

        if (empty($value)) {
            return $empty;
        }

        return is_null($length) ?
            $value :
            ((strlen($value) > $length) ?
                mb_substr($value, 0, $length) :
                $value);
    }

    /**
     * @param $attributes
     */
    protected function afterProcess($attributes)
    {
        //remove null attribute
        $attributes = array_filter($attributes, function ($item) {
            return !is_null($item);
        });

        return $attributes;
    }
}