<?php

namespace app\models\Rf\Adapter;

/**
 * Class TestModelAttributeAdapter
 * @package app\models\Rf\Adapter
 *
 * @property $rawItem
 */
class TestModelAttributeAdapter extends BaseModelAttributeAdapter
{
    /**
     * @return []
     */
    public function getAttributes()
    {
        return $this->afterProcess([
                'integer' => $this->asInteger($this->rawItem->integer),
                'integerZero' => $this->asInteger($this->rawItem->integerZero),
                'float' => $this->asFloat($this->rawItem->float),
                'floatZero' => $this->asFloat($this->rawItem->floatZero),
                'text' => $this->asText($this->rawItem->text),
                'textEmpty' => $this->asText($this->rawItem->textEmpty),
                'bool' => $this->asBoolean($this->rawItem->bool),
            ]
        );
    }
}