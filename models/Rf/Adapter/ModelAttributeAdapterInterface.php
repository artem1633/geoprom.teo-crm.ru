<?php

namespace app\models\Rf\Adapter;

interface ModelAttributeAdapterInterface
{
    /**
     * AdapterInterface constructor.
     * @param mixed $rawData
     */
    public function __construct($rawData);

    /**
     * @return []
     */
    public function getAttributes();
}