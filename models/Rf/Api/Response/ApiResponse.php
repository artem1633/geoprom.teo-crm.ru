<?php

namespace app\models\Rf\Api\Response;


abstract class ApiResponse extends BaseApiResponse
{
    /**
     * @return array
     */
    public function asArray()
    {
        return [
            'version' => $this->getVersion(),
            'data' => $this->getData(),
        ];
    }

    abstract public function getVersion();

    abstract public function getData();
}