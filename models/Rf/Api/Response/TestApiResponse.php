<?php

namespace app\models\Rf\Api\Response;


class TestApiResponse extends ApiResponse
{
    const VERSION = 'test_api';
    const DATA_ITEM = 42;

    public function getVersion()
    {
        return self::VERSION;
    }

    public function getData()
    {
        return ['item' => self::DATA_ITEM];
    }

}