<?php

namespace app\models\Rf\Api\Response;


abstract class BaseApiResponse implements ApiResponseInterface
{
    /**
     * @return array
     */
    abstract public function asArray();
}