<?php

namespace app\models\Rf\Api\Response;


interface ApiResponseInterface
{
    /**
     * @return array
     */
    public function asArray();
}