<?php

namespace app\models\Rf\Factory;

interface FactoryInterface
{
    public function create($factor);
}