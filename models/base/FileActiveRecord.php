<?php

namespace app\models\base;

/**
 * Class FileActiveRecord
 * @package app\models\base
 * @property string $path
 *
 * @property bool $isImage
 */
class FileActiveRecord extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['path'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'path' => 'Путь',
        ];
    }

    /**
     * @return bool
     */
    public function getIsImage()
    {
        return exif_imagetype($this->path) == false ? false : true;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeDelete()
    {
        if(file_exists($this->path))
        {
            unlink($this->path);
        }

        return parent::beforeDelete();
    }
}