<?php

namespace app\models\chat;

use app\models\message\Message;
use app\models\message\MessageType;
use app\models\task\Task;
use app\Rf\Modules\Users\models\User;
use yii\base\Model;
use yii\bootstrap\Html;

/**
 * Class SendMessageForm
 * @package app\models\chat
 *
 * @property-read Message $message
 */
class SendMessageForm extends Model
{
    public $userId;
    public $taskId;
    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['userId', 'taskId',], 'required'],
            [['text',], 'safe'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'text' => \Yii::t('app', 'Message text'),
        ];
    }

    /**
     * @return Message|false
     */
    public function getMessage()
    {
//        if (0 == strlen(trim($this->text))) {
//            return false;
//        }

        return new Message([
            'type' => MessageType::regular(),
            'task' => Task::findOne($this->taskId),
            'user' => User::current(),
            'text' => Html::encode(trim($this->text)),
        ]);
    }

}
