<?php

/* @var $this yii\web\View */
/* @var $model \app\models\faqPost\FaqPost */

use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;
use app\models\faqCategory\FaqCategory;
use yii\helpers\Html;

$this->title = $model->isNewRecord ? 'Создать FAQ-пост' : 'Изменить FAQ-пост';

?>

<div class="panel panel-primary">
    <div class="panel-heading">
        <div class="panel-title"><?=$this->title?></div>
    </div>
    <div class="panel-body">
        <div class="task-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'category_id')->dropDownList(FaqCategory::getList()) ?>

            <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
            ]) ?>

            <?= Html::submitButton('Готово', ['class' => 'btn btn-success pull-right']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
