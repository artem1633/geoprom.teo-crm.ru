<?php

use yii\bootstrap\Modal;
use \yii\helpers\Html;
use johnitvn\ajaxcrud\CrudAsset;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $categories \app\models\faqCategory\FaqCategory[] */

$this->title = Yii::t('app', 'Manual');
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div>

    <?php if(\app\Rf\Modules\Users\models\User::current()->isAdmin): ?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">FAQ</div>
            </div>
            <div class="panel-body">
                <?= Html::a('Добавить категорию', ['create-category'], ['class' => 'btn btn-primary', 'role' => 'modal-remote']); ?>
                <?= Html::a('Добавить пост', ['create-post'], ['class' => 'btn btn-success']); ?>
            </div>
        </div>
    <?php endif; ?>

    <?php Pjax::begin(['id' => 'faq-pjax-container', 'enablePushState' => false]) ?>

        <?php foreach($categories as $category): ?>

            <h2><?=$category->name?> <?=\app\Rf\Modules\Users\models\User::current()->isAdmin ? Html::a('<i class="fa fa-pencil"></i>', ['update-category', 'id' => $category->id], ['class' => 'text text-primary', 'role' => 'modal-remote',
                    'style' => 'font-size: 20px;']) : null?>

                <?=\app\Rf\Modules\Users\models\User::current()->isAdmin ? Html::a('<i class="fa fa-trash"></i>', ['delete-category', 'id' => $category->id], ['class' => 'text text-danger', 'role' => 'modal-remote',
                    'style' => 'font-size: 20px;', 'data-confirm' => false, 'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-toggle' => 'tooltip',
                    'data-confirm-title' => 'Вы уверены?',
                    'data-confirm-message' => 'Вы точно хотите удалить категорию?']) : null?></h2>

            <?php foreach ($category->faqPosts as $post): ?>

                <?= \yii\bootstrap\Collapse::widget(
                    ['items' => [
                        [
                            'label' => \app\Rf\Modules\Users\models\User::current()->isAdmin ? ($post->name
                             . Html::a('<i class="fa fa-trash" style="color: #a94442;"></i>', ['delete-post', 'id' => $post->id], ['class' => 'pull-right', 'role' => 'modal-remote',
                                    'style' => 'font-size: 20px; margin-left: 13px;', 'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                    'data-request-method' => 'post',
                                    'data-toggle' => 'tooltip',
                                    'data-confirm-title' => 'Вы уверены?',
                                    'data-confirm-message' => 'Вы точно хотите удалить категорию?'])
                            . Html::a('<i class="fa fa-pencil" style="color: #337ab7;"></i>', ['update-post', 'id' => $post->id], ['class' => 'pull-right', 'style' => 'font-size: 20px;'])) : $post->name,
                            'encode' => false,
                            'content' => $post->content,
                        ]]]); ?>

            <?php endforeach; ?>

        <?php endforeach; ?>

    <?php Pjax::end() ?>

</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>