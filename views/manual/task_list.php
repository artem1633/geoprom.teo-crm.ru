<?php

use yii\helpers\Html;

?>
<?= Html::img('/manual/tasks/task_row.png', ['class' => 'img-thumbnail']); ?>
<p>1. В списке есть возможность фильтровать и сортировать запросы</p>
<p>2. У запроса есть список возможных действий</p>
<?= Html::img('/manual/tasks/task_row_actions.png', ['class' => 'img-thumbnail']); ?>
<ul>
    <li>Просмотреть страницу запроса</li>
    <li>Просмотреть краткий вид запроса</li>
    <li>Изменить запрос (<span class="text-danger">только администратор</span>)</li>
    <li>Удалить запрос запрос (<span class="text-danger">только администратор</span>)</li>
</ul>