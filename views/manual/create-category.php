<?php

/* @var $model \app\models\faqCategory\FaqCategory */

use yii\widgets\ActiveForm;



?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php ActiveForm::end(); ?>

</div>
