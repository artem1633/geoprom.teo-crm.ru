<?php

use app\models\attachment\AddAttachmentsForm;

?>

<?= \app\widgets\attachment\FilesWidget::widget(['task' => $model, 'filesForm' => new AddAttachmentsForm(['taskId' => $model->id]), 'ignoreMessages' => false]) ?>