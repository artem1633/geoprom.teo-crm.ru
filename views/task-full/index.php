<?php

use johnitvn\ajaxcrud\CrudAsset;
use kartik\icons\Icon;


/* @var $this yii\web\View */
/* @var $task \app\models\task\Task */
/* @var $filesForm \app\models\attachment\AddAttachmentsForm */

$this->title = Yii::t('app', 'Task') . ' ' .
    \Yii::t('app', '#') . ' ' . $task->id;
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$script = <<< JS
$(document).ready(function() {
    function chatToBottom(){
        $("#chat-body .pre-scrollable").animate({ scrollTop: $('#chat-body .pre-scrollable > .col-sm-12').height()-$('#chat-body .pre-scrollable').height() },0);
    }
    chatToBottom();
    setInterval(function(){ 
        console.log('Send');
        $.ajax({
        url: 'index.php?r=task-full/new-messages&taskId={$task->id}',
        type: 'post',
        data: {'lastMessage': window.lastMessage},
        success: function (data) {
            if (data.new > 0) {
                $.pjax.reload('#chat-body');
                initMagnificPopup();
            }
                /*var notify = new Notification("test");*/
        }
    });
        
        
        //$("#refreshButton").click();
        }, 10000);

    $('#chat-body').on('pjax:success', function() {
        chatToBottom();
    });
  
});

$(document).on("keydown", "#sendmessageform-text", function(e)
{
    if ((e.keyCode == 10 || e.keyCode == 13) && e.ctrlKey)
    {
        if($('#sendmessageform-text').val()!= '' || $('#uploadform-file').fileinput('getFilesCount') > 0){
            $('#sendmessageform-text').closest("form").submit();
        }
    }
});

function initMagnificPopup()
{
 	$('#chat-body').magnificPopup({
	    delegate: 'a.gallery-element',
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			verticalFit: true
		}
	});   
}

initMagnificPopup();

JS;
$this->registerJs($script);

?>

<div class="task-full">

    <?php \yii\widgets\Pjax::begin(['id' => 'info', 'enablePushState' => false]); ?>

    <?= \app\widgets\task\FullTaskInfo::widget(['task' => $task]) ?>

    <?php \yii\widgets\Pjax::end(); ?>


        <?= \app\widgets\common\MyCollapse::widget(
            [
                'encodeLabels' => false,
                'itemToggleOptions' => [
                    'tag' => 'a',
                    'class' => 'custom-toggle',
                ],
                'items' => [
                    [
                        'label' => Icon::show('angle-double-down') . 'Файлы',
                        'content' => \app\widgets\attachment\FilesWidget::widget(['task' => $task, 'filesForm' => $filesForm]),
                        'options' => ['class' => 'panel-primary'],
                    ]]]); ?>


    <div class="row">

        <div class="col-sm-3 col-lg-2">

            <?php \yii\widgets\Pjax::begin(['id' => 'team', 'enablePushState' => false]); ?>

            <?= \app\widgets\task\full_task\FullTaskTeam::widget(['task' => $task]) ?>

            <?php \yii\widgets\Pjax::end(); ?>

        </div>
        <div class="col-sm-9 col-lg-10">

            <?php \yii\widgets\Pjax::begin(['id' => 'chat', 'enablePushState' => false]); ?>

            <?= \app\widgets\chat\Chat::widget(['task' => $task]) ?>

            <?php \yii\widgets\Pjax::end(); ?>

        </div>
    </div>


    <?php //var_dump($task->notifications); ?>
    <?php //var_dump($task->myNotificationCount); ?>
    <?php //var_dump($task->myNotifications); ?>

    <?php //var_dump($task); ?>


</div>



