<?php

$this->title = 'Ошибка';

?>

<div class="error-page">
    <h2 class="headline text-yellow"> <?=$exception->getCode()?> </h2>

    <div class="error-content">
        <h3><i class="fa fa-warning text-yellow"></i> Ошибка</h3>

        <p>
            <?=$exception->getMessage()?>
        </p>

    </div>
    <!-- /.error-content -->
</div>