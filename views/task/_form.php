<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\task\Task */
/* @var $form yii\widgets\ActiveForm */

/* @var $author \app\models\team\TeamMember */
/* @var $executor \app\models\team\TeamMember */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'body')->textarea(['maxlength' => true]) ?>

    <?php
    //create only
    if (!is_null($executor)) {
        if(!$model->isNewRecord){
            echo $form->field($model, 'status_id')
                ->dropDownList(\app\models\task\TaskStatus::createOptionsList());
        }

        echo $form->field($executor, 'user_id')
            ->widget(\kartik\select2\Select2::class,[
                'data' => \app\Rf\Modules\Users\models\User::filterSelfAndAdminsList(),
            ])
            ->label(Yii::t('app', 'Executor'));

        echo $form->field($model, 'observersList')->widget(\kartik\select2\Select2::class, [
            'data' => \yii\helpers\ArrayHelper::map(\app\Rf\Modules\Users\models\User::find()->all(), 'id', 'name'),
            'pluginOptions' => [
                'multiple' => true,
            ],
        ]);
    } ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
