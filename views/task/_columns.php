<?php

use app\models\message\Message;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\helpers\Url;

$setting = \app\models\settings\Settings::findByKey('table_row_line_height');

$lineHeight = $setting->value * 10;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'hiddenFromExport' => true,
    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\app\widgets\columns\DataColumn',
        'attribute' => 'id',
        'contentOptions' => ['style' => 'height: 100%; line-height: '.$lineHeight.'px;'],
        'hiddenFromExport' => true,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'flags',
        'hidden' => true,
        'hiddenFromExport' => true,
        'format' => 'raw',
        'value' => function(){
            return null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'label' => 'Номер (ID)',
        'visible' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'label' => 'Заведено',
        'hidden' => true,
        'content' => function($model){
            return (new \DateTime($model->created_at))->format('d-m-Y');
        }
    ],

    [ // For HTML
        'class' => '\app\widgets\columns\DataColumn',
        'attribute' => 'title',
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \app\widgets\task\TitleLink::widget(['task' => $task]);
        },
        'hiddenFromExport' => true,
    ],
    [ // For Export
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'title',
        'format' => 'raw',
        'contentOptions' => ['style' => 'height: 100%;'],
        'visible' => false,
    ],
    [
        'class' => '\app\widgets\columns\DataColumn',
        'attribute' => 'body',
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \app\widgets\task\ShortBody::widget(['task' => $task]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'myTeamRoleId',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterInputOptions' => [
            'placeholder' => 'Выберите значение',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filter' => \app\Rf\Modules\Users\models\User::current()->role->getMyTeamRoleFilter(),
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \yii\bootstrap\Html::tag('span', $task->myTeamRole->label, ['class' => $task->myTeamRole->labelClass]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'messagesList',
        'label' => 'Сообщений',
        //'filter' => \app\Rf\Modules\Users\models\User::current()->role->getMyTeamRoleFilter(),
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return Html::a(count($task->messagesList), '#', [
                'data-toggle' => 'popover',
                'data-trigger' => 'hover',
                'data-html' => 'true',
                'data-content' => (new \app\widgets\chat\Chat(['task' => $task]))->getSmall(),
            ]);
        },
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'messagesList',
        'label' => 'Вложений',
        //'filter' => \app\Rf\Modules\Users\models\User::current()->role->getMyTeamRoleFilter(),
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            $messages = Message::find()->where(['task_id' => $task->id])->with('files')->all();

            $messagesPreview = [];

            foreach ($messages as $message) {
                $messagesPreview = \yii\helpers\ArrayHelper::merge($messagesPreview, \yii\helpers\ArrayHelper::getColumn($message->getFiles()->all(), 'path'));
            }

            return Html::a(count($messagesPreview) + count($task->attachments), ['task-full/attachments', 'id' => $task->id], [
                'role' => 'modal-remote'
            ]);
        },
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],
    [ // For HTML
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'authorId',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterInputOptions' => [
            'placeholder' => 'Выберите значение',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filter' => \app\Rf\Modules\Users\models\User::allList(),
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \app\widgets\user\UserInfo::widget(['user' => $task->author]);
        },
        'hiddenFromExport' => true,
    ],
    [ // For Export
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'authorId',
        'filter' => \app\Rf\Modules\Users\models\User::allList(),
        'value' => 'author.name',
        'visible' => false,
    ],
    [ // For HTML
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'executorId',
        'filter' => \app\Rf\Modules\Users\models\User::allList(),
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterInputOptions' => [
            'placeholder' => 'Выберите значение',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \app\widgets\user\UserInfo::widget(['user' => $task->executor]);
        },
        'hiddenFromExport' => true,
    ],
    [ // For Export
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'executorId',
        'filter' => \app\Rf\Modules\Users\models\User::allList(),
        'format' => 'raw',
        'value' => 'executor.name',
        'visible' => false,
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status_id',
        'filter' => \app\models\task\TaskStatus::list(),
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filterInputOptions' => [
            'placeholder' => 'Выберите значение',
        ],
        'filterWidgetOptions' => [
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'format' => 'raw',
        'value' => function (\app\models\task\Task $task) {
            return \app\widgets\task\TaskStatus::widget(['task' => $task]);
        },
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'value' => function(\app\models\task\Task $task){
//            /** @var \app\models\team\TeamMember $member */
//            $member = \app\models\team\TeamMember::find()->where(['task_id' => $task->id, 'user_id' => \app\Rf\Modules\Users\models\User::current()->id])->one();
//            if($member->hasUnreadMessages){
//                return 'yes';
//            } else {
//                return 'no';
//            }
//        },
//    ],
    [
        'class' => '\kartik\grid\DataColumn',

        'attribute' => 'created_at',
        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
//                'timePicker' => true,
//                'timePickerIncrement' => 15,
                'timePicker24Hour' => true,
                'locale' => [
                    'format' => 'YYYY-MM-DD H:mm'
                ],
            ],
        ],
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'value' => 'executionTime',
        'label' => 'Время исполнения (ч)',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'vAlign' => \kartik\grid\GridView::ALIGN_CENTER,
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'finished_at',
        'filterType' => \kartik\grid\GridView::FILTER_DATE_RANGE,
        'filterWidgetOptions' => [
            'pluginOptions' => [
//                'timePicker' => true,
//                'timePickerIncrement' => 15,
                'timePicker24Hour' => true,
                'locale' => [
                    'format' => 'YYYY-MM-DD H:mm'
                ],
            ],
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => \app\Rf\Modules\Users\models\User::current()->isAdmin ? '{full-view} {view} {update} {delete}' : '{full-view} {view}',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'full-view' => function ($url, $task) {
                return \yii\bootstrap\Html::a('<span class="fa fa-search"></span>',
                    Url::toRoute(['/task-full', 'taskId' => $task->id]),
                    ['data-pjax' => 0, 'title' => Yii::t('app', 'Full view'),]);
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip'],

        'updateOptions' =>
            ['role' => 'modal-remote', 'title' => Yii::t('app', 'Edit'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'Delete'),
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы действительно хотите удалить данный запрос?'],
    ],

];   