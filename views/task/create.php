<?php


/* @var $this yii\web\View */
/* @var $model app\models\task\Task */
/* @var $author \app\models\team\TeamMember */
/* @var $executor \app\models\team\TeamMember */

?>
<div class="task-create">
    <?= $this->render('_form', [
        'model' => $model,
        'executor' => $executor,
    ]) ?>
</div>
