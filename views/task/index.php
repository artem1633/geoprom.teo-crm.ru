<?php

use app\models\message\MessageType;
use app\widgets\wrapper\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\task\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tasks');
$this->params['breadcrumbs'][] = $this->title;

$setting = \app\models\settings\Settings::findByKey('table_row_line_height');

CrudAsset::register($this);

$newTasksClass = $searchModel->flagEnabled(\app\models\task\TaskSearch::FLAG_NEW_TASKS) ? 'btn btn-success' : 'btn btn-default';
$newMessagesClass = $searchModel->flagEnabled(\app\models\task\TaskSearch::FLAG_NEW_MESSAGES) ? 'btn btn-success' : 'btn btn-default';


/**
 * @param \app\models\task\Task $task
 * @param bool
 */
function hasUnreadMessages($task)
{
    $myMember = $task->getTeamMembers()->where(['user_id' => Yii::$app->user->getId()])->one();
    $message = $task->getMessages()->andWhere(['type_id' => MessageType::REGULAR])->orderBy('created_at DESC')->one();

    if($message && $myMember)
    {
        $messageDateTime = strtotime($message->created_at);

        $lastMemberOnline = strtotime($myMember->task_seen);
            if(($lastMemberOnline - $messageDateTime) < 0){
                return true;
            } else {
                return false;
            }
    }

    return true;
}

/**
 * @param \app\models\task\Task $task
 * @param bool
 */
function isMember($task)
{
    $myMember = $task->getTeamMembers()->where(['user_id' => Yii::$app->user->getId()])->one();

    if($myMember) {
        return $myMember->team_role_id != \app\models\team\TeamRole::FOREIGN;
    }

    return false;
}

/**
 * @param \app\models\task\Task $task
 * @param bool
 */
function isExecutor($task)
{
    $myMember = $task->getTeamMembers()->where(['user_id' => Yii::$app->user->getId()])->one();

    if($myMember) {
        return $myMember->team_role_id == \app\models\team\TeamRole::EXECUTOR;
    }

    return false;
}

/**
 * Проверяет текущего пользователя на последние отправителя в чате задачи
 * @param \app\models\task\Task $task
 * @param bool
 */
function isMeLastSend($task)
{
    $message = $task->getMessages()->andWhere(['type_id' => MessageType::REGULAR])->orderBy('created_at DESC')->one();

    return $message->user_id == Yii::$app->user->getId();
}

?>
<div class="task-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'rowOptions' => function ($model) {

                $additionalStatus = null;

                $isMeLastSend = isMeLastSend($model);

                if(isMember($model) == false){
                    $additionalStatus = '';
                }else if($isMeLastSend) {
                    $additionalStatus = ' my_last_message';
                } else {
                    $additionalStatus = ' member_unread';
                    if(isExecutor($model) && hasUnreadMessages($model)){
                        $additionalStatus = ' member_unread_warning';
                    }
                }

                return ['class' => "trStatus_" . $model->status_id . ' myrole' . $model->myTeamRole->id . $additionalStatus, 'data-task-row-id' => $model->id];
            },
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                ['content' =>
                    Html::a('Новые сообщения', '#', ['class' => $newMessagesClass, 'data-filter-flag' => 'new_messages']) .
                    Html::a('Новые заявки', '#', ['class' => $newTasksClass, 'data-filter-flag' => 'new_tasks']) .
                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                        ['role' => 'modal-remote', 'title' => Yii::t('app', "Create new task"), 'class' => 'btn btn-default']) .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => Yii::t('app', "Reset Grid"),]) .
                    '{toggleData}' .
                    '{export}' .
                    '<div class="toolbar-input-wrapper">'.Html::activeInput('number', $setting, 'value', ['id' => 'input-row-line-height', 'class' => 'toolbar-input']).'</div>'
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary',
                'heading' => '<i class="glyphicon glyphicon-list"></i> ' . Yii::t('app', 'Tasks'),
                //'before' => '<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                'after' => (\app\Rf\Modules\Users\models\User::current()->isAdmin ? BulkButtonWidget::widget([
                        'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; ' . Yii::t('app', 'Delete All'),
                            ["bulk-delete"],
                            [
                                "class" => "btn btn-primary btn-xs",
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false, 'data-method' => false,// for overide yii data api
                                'data-request-method' => 'post',
                                'data-confirm-title' => 'Are you sure?',
                                'data-confirm-message' => 'Are you sure want to delete this item'
                            ]),
                    ]) : null).
                    '<div class="clearfix"></div>',
            ]
        ]) ?>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    'options' => [
        'tabindex' => false,
    ],
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php

$changeUrl = \yii\helpers\Url::toRoute(['settings/change-setting-value-ajax']);

$script = <<< JS

    var csrfParam = $('meta[name="csfr-param"]').attr('content');
    var csrfToken = $('meta[name="csfr-token"]').attr('content');

    function setEventListeners()
    {
        $('#input-row-line-height').change(function(){
       
           var value = $(this).val();
           
           $.ajax({
              type: "POST",
              url: "{$changeUrl}",
              data: {
                  'key': 'table_row_line_height',
                  'value': value,
                  csrfParam: csrfToken,
              },
              success: function(){
                  $('#crud-datatable').yiiGridView('applyFilter');
              },
            });
       });
        
       $('[data-filter-flag]').click(function(){
           var flag = $(this).data('filter-flag');
           
           if($('input[name="TaskSearch[flags]"]').val() == '')
           {
               var values = [];
           } else {
               var values = $('input[name="TaskSearch[flags]"]').val().split(","); 
           }
           console.log('Values', values);
           var newValues = [];
           var wasFlag = false;
           $.each(values, function(key, value){
               if(value != flag)
               {     
                newValues.push(value);
               } else {
                   wasFlag = true;
               }
           });
           
           if(wasFlag == false){
               newValues.push(flag);
           }
            
           console.log('New values', newValues);
           
           $('[data-filter-flag]').each(function(){
               var flag = $(this).data('filter-flag');
               
               if(newValues.indexOf(flag) != -1)
               {
                    $(this).removeClass('btn-default');   
                    $(this).addClass('btn-success');   
               } else {
                   $(this).addClass('btn-default');   
                    $(this).removeClass('btn-success');   
               }
           });
           
           var str = newValues.join(',');
           
           $('input[name="TaskSearch[flags]"]').attr('value', str);
           setTimeout(function(){
               $('#crud-datatable').yiiGridView('applyFilter');
           } ,500);
       });
    }
    
    function initPopovers()
    {
        $('[data-toggle="popover"]').popover({ trigger: "manual" , html: true, animation:false}).on('mouseenter', function () {
            var _this = this;
            $(this).popover('show');
            $('.popover').on('mouseleave', function () {
                $(_this).popover('hide');
            });
        }).on('mouseleave', function () {
            var _this = this;
            setTimeout(function () {
                if (!$('.popover:hover').length) {
                    $(_this).popover('hide');
                }
            }, 300);
        });
    }
    
    setEventListeners();
    initPopovers();
    
    $(document).on("pjax:success", '#crud-datatable-pjax', function(){
        setEventListeners();
        initPopovers();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>