<?php

/* @var $this yii\web\View */
/* @var $model app\models\task\Task */
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
        'executor' => null,
    ]) ?>

</div>
