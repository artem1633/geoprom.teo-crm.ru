<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\task\Task */
?>
<div class="task-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'body',
            //'status_id',
            [
                'label' => Yii::t('app', 'Status'),
                'format' => 'raw',
                'value' => \app\widgets\task\TaskStatus::widget(['task' => $model]),
            ],
            'created_at',
            'finished_at',
        ],
    ]) ?>

</div>
