<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\Button;
use yii\bootstrap\Alert;

?>

<?$footer = Button::widget([
    'label' => 'Удалить',
    'options' => [
        'class' => 'btn-lg btn-default',
        'style' => 'margin:5px',
        'id' => 'delete-record',
    ]
]);
$footer .= Button::widget([
    'label' => 'Отмена',
    'options' => [
        'class' => 'btn-lg btn-default',
        'style' => 'margin:5px'
    ]
]);
?>
<?php
Modal::begin([
    'id' => 'modal-controll-message',
    'footer' => $footer,
    'size'   => 'model-sm',
]);

?>
<div id="modal-content">
<?=Alert::widget([
    'body' => '<h4><span class="glyphicon glyphicon-info-sign kv-alert-title"></span> <span class="kv-alert-title">Вы действительно хотите удалить запись</span></h4>Для продолжения нажмите "Удалить". Для отмены нажмите "Отмена" или закройте окно',
    'options' => [
                    'class' => 'alert-danger',
                ],
    'closeButton' => false,
])?>
</div>
<?php
Modal::end();
?>



