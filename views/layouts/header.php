<?php

use app\Rf\Modules\Users\models\User;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

/**
 * @var \app\Rf\Modules\Users\models\User $identity
 */
$identity = Yii::$app->user->identity;
if ($identity) {
    $auth = Html::a(
        'Выйти',
        ['/users/auth/logout'],
        ['data-method' => 'post', 'class' => 'dropdown-toggle']);
} else {
    $auth = Html::a(
        '<strong id="user-name">Авторизуйтесь</strong>',
        ['/users/auth/login'],
        ['data-method' => 'get', 'class' => 'dropdown-toggle']);
}
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">' . strtoupper(Yii::$app->name[0]) . '</span><span class="logo-lg">' .
        Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>


        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <?php if ($identity): ?>

                    <?= \app\widgets\notification\HeaderDropdown::widget([
                        'user' => \app\Rf\Modules\Users\models\User::current()
                    ]) ?>


                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <span class="hidden-xs"><?= User::current()->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <p>
                                    <?= User::current()->name ?>
                                    <small><?= User::current()->role->label ?></small>
                                </p>
                            </li>
                            <!-- Menu Body -->
                            <li class="user-body">
                                <?= \app\widgets\user\UserInfo::widget(['user' => User::current()]) ?>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">

                            </li>
                        </ul>
                    </li>

                <?php endif; ?>


                <li class="dropdown user user-menu">
                    <?= $auth ?>
                </li>

                <!-- User Account: style can be found in dropdown.less -->

            </ul>
        </div>
    </nav>
</header>
