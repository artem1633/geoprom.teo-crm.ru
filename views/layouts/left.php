<?php
//function menuAccess($roles)
//{
//    foreach($roles as $role){
//        if (Yii::$app->user->identity->{$role})
//            return false;
//    }
//    return true;
//}

$items = [
    ['label' => 'Меню', 'options' => ['class' => 'header']],
];
/**
 * @var \app\Rf\Modules\Users\models\User $identity
 */
$identity = Yii::$app->user->identity;
$items = array_merge($items, $identity->roleDependent->menuItems);
//['label' => Yii::t('users', 'Users'), 'icon' => 'users', 'url' => ['/users/user']];
//$items = [
//    ['label' => Yii::t('users', 'Users'), 'icon' => 'users', 'url' => ['/users/user']]
//]
//$items = array_merge($items,menuAccess(['isGager','isForemanScreed'])?[['label' => 'Штукатурка', 'icon' => 'home', 'url' => ['/projects']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isForemanPlastering'])?[['label' => 'Стяжка', 'icon' => 'home', 'url' => ['/project-screed']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed','isForeman'])?[['label' => 'Баланс', 'icon' => 'money', 'url' => ['/balance']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed'])?[['label' => 'Материалы для штукатурки', 'icon' => 'file-word-o', 'url' => ['/project-materials-type']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed'])?[['label' => 'Материалы для стяжки', 'icon' => 'file-word-o', 'url' => ['/project-screed-materials-type']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed'])?[['label' => 'Виды работ', 'icon' => 'gavel', 'url' => ['/project-params-type']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForeman'])?[['label' => 'Виды работ стяжка', 'icon' => 'gavel', 'url' => ['/project-screed-params-type']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed'])?[['label' => 'Доп.расходы', 'icon' => 'money', 'url' => ['/project-add-materials']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForemanScreed'])?[['label' => 'Виды доп.расходов', 'icon' => 'file-code-o', 'url' => ['/project-add-materials-type']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForeman'])?[['label' => 'Пользователи', 'icon' => 'user', 'url' => ['/users']]]:[]);
//$items = array_merge($items,menuAccess(['isGager','isOnlyManager','isForeman'])?[['label' => 'Бригады', 'icon' => 'users', 'url' => ['/workers']]]:[]);
//$items = array_merge($items,menuAccess(['isForeman'])?[['label' => 'Монтаж штукатурки', 'icon' => 'columns', 'url' => ['/plastering']]]:[]);
//$items = array_merge($items,menuAccess(['isForeman'])?[['label' => 'Монтаж стяжки', 'icon' => 'columns', 'url' => ['/screed']]]:[]);
//$items = array_merge($items,menuAccess(['isForeman'])?[['label' => 'Замеры', 'icon' => 'map-pin', 'url' => ['/measurement']]]:[]);
//$items = array_merge($items,menuAccess(['isForeman'])?[['label' => 'Зарплата', 'icon' => 'rub', 'url' => ['/salary']]]:[]);
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => $items
            ]
        ) ?>

    </section>

</aside>
