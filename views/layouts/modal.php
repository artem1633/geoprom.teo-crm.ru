<?php
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\bootstrap\Button;

?>

<?php
Modal::begin([
    'id' => 'modal-controll-form',
    // 'header' => '<h3>Выберите период</h3>',
    'toggleButton' => [
         'label' => '',
         'id' => 'button-period',
         ],
    'footer' => false,
    'size'   => 'model-sm',
]);

?>
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Default Modal</h4>
    </div>
    <div class="modal-body">
        <p>One fine body&hellip;</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
    </div>
</div>


<?php
Modal::end();
?>



