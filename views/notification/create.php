<?php


/* @var $this yii\web\View */
/* @var $model app\models\notification\Notification */

?>
<div class="notification-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
