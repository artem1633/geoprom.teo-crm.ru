<?php

use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'important',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'message',
        'format' => 'html'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'taskTitle',
        'label' => Yii::t('app', 'Task'),
        'format' => 'raw',
        'value' => function (\app\models\notification\Notification $notification) {
            return \app\widgets\task\TitleLink::widget(['task' => $notification->task]);
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'event_id',
        'format' => 'raw',
        'filter' => \app\models\task\TaskEvent::list(),
        'value' => function (\app\models\notification\Notification $notification) {
            return $notification->event->label;
        }
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'event_id',
//    ],

//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'task_id',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'user_id',
//        'label' => 'Для кого',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{full-view} {delete}',
        'buttons' => [
            'full-view' => function ($url, \app\models\notification\Notification $notification) {
                return \yii\bootstrap\Html::a('<span class="fa fa-search"></span>',
                    Url::toRoute(['/task-full', 'taskId' => $notification->task->id]),
                    ['data-pjax' => 0, 'title' => Yii::t('app', 'Full view'),]);
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'View'), 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'Edit'), 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => Yii::t('app', 'Delete'),
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Вы уверены что хотите удалить данное уведомление'],
    ],

];   