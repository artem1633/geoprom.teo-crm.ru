<?php

/* @var $this yii\web\View */
/* @var $model app\models\notification\Notification */
?>
<div class="notification-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
