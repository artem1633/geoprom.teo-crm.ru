<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\notification\Notification */
?>
<div class="notification-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'important',
            'message',
            'event_id',
            'task_id',
            'user_id',
            'created_at',
        ],
    ]) ?>

</div>
