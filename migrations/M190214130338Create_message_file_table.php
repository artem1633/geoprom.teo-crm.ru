<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `message_file`.
 */
class M190214130338Create_message_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('message_file', [
            'id' => $this->primaryKey(),
            'message_id' => $this->integer()->comment('Сообщение'),
            'name' => $this->string()->comment('Наименование'),
            'path' => $this->string()->comment('Путь')
        ]);

        $this->createIndex(
            'idx-message_file-message_id',
            'message_file',
            'message_id'
        );

        $this->addForeignKey(
            'fk-message_file-message_id',
            'message_file',
            'message_id',
            'message',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-message_file-message_id',
            'message_file'
        );

        $this->dropIndex(
            'idx-message_file-message_id',
            'message_file'
        );

        $this->dropTable('message_file');
    }
}
