<?php

namespace app\migrations;

use app\models\settings\Settings;
use yii\db\Migration;

/**
 * Class M190517181034Add_friday_setting
 */
class M190517181034Add_friday_setting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'friday_work_hours',
            'value' => '8',
            'label' => 'Кол-во рабочих часов в пятницу',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
