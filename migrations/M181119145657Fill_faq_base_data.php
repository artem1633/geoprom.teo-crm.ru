<?php

namespace app\migrations;

use Yii;
use yii\db\Migration;
use \app\models\faqCategory\FaqCategory;
use \app\models\faqPost\FaqPost;

/**
 * Class M181119145657Fill_faq_base_data
 */
class M181119145657Fill_faq_base_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $mainMenu = new FaqCategory(['name' => 'Главное меню']);
        $mainMenu->save();

        $request = new FaqCategory(['name' => 'Запросы']);
        $request->save();

        $notifications = new FaqCategory(['name' => 'Уведомления']);
        $notifications->save();

        $users = new FaqCategory(['name' => 'Пользователи']);
        $users->save();

        $elementsPost = new FaqPost([
            'name' => 'Элементы',
            'category_id' => $mainMenu->id,
            'content' => Yii::$app->view->render('@app/base_data/faq/main_menu_items'),
        ]);
        $elementsPost->save();

        $newRequest = new FaqPost([
            'name' => 'Создание нового запроса',
            'category_id' => $request->id,
            'content' => Yii::$app->view->render('@app/base_data/faq/task_create'),
        ]);
        $newRequest->save();

        $requestList = new FaqPost([
            'name' => 'Работа со списком запросов',
            'category_id' => $request->id,
            'content' => Yii::$app->view->render('@app/base_data/faq/task_list'),
        ]);
        $requestList->save();

        $requestPage = new FaqPost([
            'name' => 'Страница запроса (полная)',
            'category_id' => $request->id,
            'content' => Yii::$app->view->render('@app/base_data/faq/task_full'),
        ]);
        $requestPage->save();

        $notificationsPost = new FaqPost([
            'name' => 'Работа с уведомлениями пользователя',
            'category_id' => $notifications->id,
            'content' => Yii::$app->view->render('@app/base_data/faq/notification_review'),
        ]);
        $notificationsPost->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return false;
    }
}
