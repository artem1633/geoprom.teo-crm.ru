<?php

namespace app\migrations;

use yii\db\Migration;

class m181024_100000_create_attachment_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('attachment', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'file' => $this->string(256)->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');


        $this->addForeignKey(
            'fk-attachment_user_id',
            'attachment',
            'user_id',
            'rf_user',
            'id'
        );

        $this->addForeignKey(
            'fk-attachment_task_id',
            'attachment',
            'task_id',
            'task',
            'id'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-attachment_task_id',
            'attachment'
        );

        $this->dropForeignKey(
            'fk-attachment_user_id',
            'attachment'
        );

        $this->dropTable('attachment');

        return true;
    }

}
