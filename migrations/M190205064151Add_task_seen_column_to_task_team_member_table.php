<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles adding task_seen to table `task_team_member`.
 */
class M190205064151Add_task_seen_column_to_task_team_member_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task_team_member','task_seen', $this->dateTime()->comment('Последний раз смотрел задачу'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('task_team_member','task_seen');
    }
}
