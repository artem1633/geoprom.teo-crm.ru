<?php

namespace app\migrations;

use yii\db\Migration;
use  app\Rf\Modules\Users\models\User;

class m181001_100000_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;


//        $this->createTable(User::tableName(), [
//            'id' => $this->primaryKey(),
//            'login' => $this->string()->unique()->notNull(),
//            'name' => $this->string(256)->notNull(),
//
//            'access_token' => $this->string(1024)->notNull(),
//            'access_key' => $this->string(1024)->notNull(),
//
//            'role_id' => $this->integer()->notNull(),
//            'password_hash' => $this->string()->notNull(),
//
//            'created_at' => $this->dateTime(),
//            'updated_at' => $this->dateTime(),
//
//        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
//
//
//        $user = new User([
//            'login' => 'admin',
//            'name' => 'Администратор',
//            'role' => \app\Rf\Modules\Users\models\Role::admin(),
//        ]);
//        $user->setPassword('admin');
//        $user->save();
//
//        $user = new User([
//            'login' => 'user01',
//            'name' => 'Расширенный пользователь',
//            'role' => \app\Rf\Modules\Users\models\Role::advancedUser(),
//        ]);
//        $user->setPassword('user01');
//        $user->save();
//
//        $user = new User([
//            'login' => 'user02',
//            'name' => 'Пользователь',
//            'role' => \app\Rf\Modules\Users\models\Role::user(),
//        ]);
//        $user->setPassword('user02');
//        $user->save();
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(User::tableName());

        return true;
    }

}
