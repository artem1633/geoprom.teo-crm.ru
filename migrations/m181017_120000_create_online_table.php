<?php

namespace app\migrations;

use yii\db\Migration;

class m181017_120000_create_online_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('online', [
            'id' => $this->primaryKey(),
            'seen' => $this->timestamp()->defaultExpression('NOW()'),
            'user_id' => $this->integer()->notNull()->unique(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');


        $this->addForeignKey(
            'fk-online_user_id',
            'online',
            'user_id',
            'rf_user',
            'id'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-online_user_id',
            'online'
        );

        $this->dropTable('online');

        return true;
    }

}
