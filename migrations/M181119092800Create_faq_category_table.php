<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `faq_category`.
 */
class M181119092800Create_faq_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faq_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование категории'),
            'created_at' => $this->dateTime()->comment('Создано'),
            'updated_at' => $this->dateTime()->comment('Изменено'),
        ]);
        $this->addCommentOnTable('faq_category', 'Категории FAQ-постов');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('faq_category');
    }
}
