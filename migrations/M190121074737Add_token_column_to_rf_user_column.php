<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Class M190121074737Add_token_column_to_rf_user_column
 */
class M190121074737Add_token_column_to_rf_user_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rf_user', 'token', $this->string()->after('name')->comment('Push-токен'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rf_user', 'token');
    }
}
