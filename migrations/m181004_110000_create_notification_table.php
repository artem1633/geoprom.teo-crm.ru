<?php

namespace app\migrations;

use yii\db\Migration;

class m181004_110000_create_notification_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('notification', [
            'id' => $this->primaryKey(),
            'important' => $this->boolean()->defaultValue(false),
            'message' => $this->string(1024),
            'event_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),

        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');


        $this->createIndex(
            'idx-notification-user_id',
            'notification',
            'user_id'
        );

        $this->addForeignKey(
            'fk-notification_user_id',
            'notification',
            'user_id',
            'rf_user',
            'id'
        );

        $this->createIndex(
            'idx-notification-task_id',
            'notification',
            'task_id'
        );

        $this->addForeignKey(
            'fk-notification_task_id',
            'notification',
            'task_id',
            'task',
            'id'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-notification_user_id',
            'notification'
        );

        $this->dropForeignKey(
            'fk-notification_task_id',
            'notification'
        );

        $this->dropTable('notification');

        return true;
    }

}
