<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `faq_post`.
 */
class M181119092856Create_faq_post_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('faq_post', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->comment('Категория FAQ-поста'),
            'name' => $this->string()->comment('Наименование поста'),
            'content' => $this->text()->comment('Содержимое поста'),
            'created_at' => $this->dateTime()->comment('Создано'),
            'updated_at' => $this->dateTime()->comment('Изменено'),
        ]);
        $this->addCommentOnTable('faq_post', 'FAQ-посты');

        $this->createIndex(
            'idx-faq_post-category_id',
            'faq_post',
            'category_id'
        );

        $this->addForeignKey(
            'fk-faq_post-category_id',
            'faq_post',
            'category_id',
            'faq_category',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-faq_post-category_id',
            'faq_post'
        );

        $this->dropIndex(
            'idx-faq_post-category_id',
            'faq_post'
        );

        $this->dropTable('faq_post');
    }
}
