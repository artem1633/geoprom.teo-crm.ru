<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles adding password_visible to table `rf_user`.
 */
class M190129154044Add_password_visible_column_to_rf_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('rf_user', 'password_visible', $this->string()->comment('Пароль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('rf_user', 'password_visible');
    }
}
