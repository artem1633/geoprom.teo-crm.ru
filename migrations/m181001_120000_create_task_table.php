<?php

namespace app\migrations;

use yii\db\Migration;

class m181001_120000_create_task_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'title' => $this->string(2048)->notNull(),
            'body' => $this->string(2048)->notNull(),
            'status_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'finished_at' => $this->timestamp()->null(),

        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('task');

        return true;
    }

}
