<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class M190202165652Create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'type' => $this->string()->defaultValue('text')->comment('Тип поля'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'table_row_line_height',
            'value' => '15',
            'label' => 'Высота строки в таблице (пиксели)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('settings');
    }
}
