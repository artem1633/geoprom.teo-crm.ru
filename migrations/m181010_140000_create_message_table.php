<?php

namespace app\migrations;

use yii\db\Migration;

class m181010_140000_create_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('message', [
            'id' => $this->primaryKey(),
            'text' => $this->string(1024)->notNull(),
            'type_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),

        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');


        $this->addForeignKey(
            'fk-message_user_id',
            'message',
            'user_id',
            'rf_user',
            'id'
        );

        $this->addForeignKey(
            'fk-message_task_id',
            'message',
            'task_id',
            'task',
            'id'
        );

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-message_user_id',
            'message'
        );

        $this->dropForeignKey(
            'fk-message_task_id',
            'message'
        );

        $this->dropTable('message');

        return true;
    }

}
