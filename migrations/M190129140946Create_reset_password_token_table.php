<?php

namespace app\migrations;

use yii\db\Migration;

/**
 * Handles the creation of table `reset_password_token`.
 */
class M190129140946Create_reset_password_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('reset_password_token', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'token' => $this->string()->comment('Токен'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-reset_password_token-user_id',
            'reset_password_token',
            'user_id'
        );

        $this->addForeignKey(
            'fk-reset_password_token-user_id',
            'reset_password_token',
            'user_id',
            'rf_user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-reset_password_token-user_id',
            'reset_password_token'
        );

        $this->dropIndex(
            'idx-reset_password_token-user_id',
            'reset_password_token'
        );

        $this->dropTable('reset_password_token');
    }
}
