<?php

namespace app\controllers;

use app\models\ContactForm;
use app\models\LoginForm;
use app\Rf\Modules\Users\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['users/auth/login']);
        }
        return $this->redirect(['/task']);
    }

    public function actionTestPush()
    {
        $usersToken = array_values(ArrayHelper::map(User::find()->where(['!=', 'token', null])->all(), 'id', 'token'));

        $result = Yii::$app->pusher
            ->addReceivers($usersToken)
            ->compose([
                'title' => "Тестовый пуш",
                'body' => "Текст тестового пуша",
                'icon' => '',
                'click_action' => '#',
            ])->send();

        return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
    }

    /**
     * Сохраняет FCM токен авторизованному пользователю
     * @return mixed
     */
    public function actionSaveToken()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $post = Yii::$app->request->post();

        if(isset($post['token'])){
            $user->token = $post['token'];
            $user->save();
        }

        return $user->getFirstErrors();
    }
}
