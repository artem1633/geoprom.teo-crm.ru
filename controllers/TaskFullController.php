<?php

namespace app\controllers;

use app\models\AddFilesForm;
use app\models\attachment\AddAttachmentsForm;
use app\models\behaviors\UpdateOnlineBehavior;
use app\models\chat\SendMessageForm;
use app\models\forms\UploadForm;
use app\models\message\Message;
use app\models\message\MessageFile;
use app\models\notification\Notification;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\models\task\TaskStatus;
use app\models\team\TeamMember;
use app\Rf\Modules\Users\models\User;
use app\widgets\chat\Chat;
use app\widgets\task\full_task\FullTaskTeam;
use app\widgets\task\FullTaskInfo;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskFullController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'updateOnline' => UpdateOnlineBehavior::class,
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'new-messages' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @var Task $task
     */
    protected $task;

    protected function seenTask()
    {
        //delete notification for task for this user
        Notification::deleteAll([
            'task_id' => $this->task->id,
            'user_id' => User::current()->id,
        ]);

        //change status WAIT=>SEEN if user is executor
        if ($this->task->status === TaskStatus::wait() &&
            $this->task->isExecutor(User::current())
        ) {
            $this->task->status = TaskStatus::seen();
            $this->task->save();

            TaskEvent::changeStatus()->trigger($this->task, User::current());
        }
    }

    public function actionMessageDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if(Yii::$app->user->identity->getIsAdmin() == false)
        {
            throw new ForbiddenHttpException('Доступ запрещен');
        }

        $message = Message::findOne($id);

        if($message != null){
            $message->delete();

            return ['result' => true];
        }

        return ['result' => false, 'error' => 'Message not found'];
    }

    public function actionChangeStatus()
    {
        if (!\Yii::$app->request->isPost) {
            return;
        }

        $taskId = \Yii::$app->request->post('taskId');
        $task = Task::findOne($taskId);

        $statusId = \Yii::$app->request->post('statusId');

        $newStatus = TaskStatus::getInstance($statusId);

        if ($task->status !== $newStatus) {
            $task->status = $newStatus;
            if ($task->save()) {
                TaskEvent::changeStatus()->trigger($task, User::current());
            }
        }

        return FullTaskInfo::widget(['task' => $task]);
    }


    public function actionAddObserver()
    {
        if (!\Yii::$app->request->isPost) {
            return;
        }

        $taskId = \Yii::$app->request->post('TeamMember')['task_id'];
        $task = Task::findOne($taskId);

        $observer = TeamMember::observer(null, $task);
        if ($observer->load(\Yii::$app->request->post())) {
            if ($observer->save()) {
                TaskEvent::observerAdd()->trigger($task, User::current(), [
                    'observer' => $observer,
                ]);
            }
        }
        return FullTaskTeam::widget(['task' => $task]);
    }

    public function actionAttachments($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = Task::findOne($id);
        if($model == null){
            throw new NotFoundHttpException();
        }

        return [
            'title' => 'Вложения запроса #',
            'content' => $this->renderAjax('attachments', ['model' => $model]),
            'footer' => Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default btn-block', 'data-dismiss' => 'modal'])
        ];
    }


    /**
     * @return string|void
     * @throws \Exception
     */
    public function actionSendMessage()
    {
        if (!\Yii::$app->request->isPost) {
            return;
        }

        $sendMessageForm = new SendMessageForm();
        $sendMessageForm->load(\Yii::$app->request->post());

//        echo '123';

        $uploadModel = new UploadForm(['uploadPath' => 'uploads/chat/', 'file' => $_FILES['UploadForm']]);
        $uploadModel->file = UploadedFile::getInstances($uploadModel, 'file');

        $task = Task::findOne($sendMessageForm->taskId);
        if ($sendMessageForm->validate()) {
//            echo '456';
            $message = $sendMessageForm->message;
            $user = $message->user;
            if ($message->text != null || !empty($uploadModel->file)) {
//                echo '789';
                $message->save();

                // Сохраняем фалы если таковые были загружены
                if($uploadModel->upload()){
                    if($uploadModel->fullUploadedFilePath != null)
                    {
                        foreach ($uploadModel->fullUploadedFilePath as $file)
                        {
                            $messageFile = new MessageFile([
                                'message_id' => $message->id,
                                'name' => $file['name'],
                                'path' => $file['path']
                            ]);
                            $messageFile->save(false);
                        }
                    }
                }

                if ($task->getStatus() === TaskStatus::seen() &&
                    $task->isExecutor(User::current())
                ) {
                    //change status BACK if user is executor
                    $task->setStatus(TaskStatus::back());
                    $task->save();

                    TaskEvent::changeStatus()->trigger($task, User::current());
                } else if ($task->getStatus() === TaskStatus::back() &&
                    $task->isAuthor(User::current())
                ) {
                    //change status WAIT if user is Author
                    $task->setStatus(TaskStatus::wait());
                    $task->save();
                    TaskEvent::changeStatus()->trigger($task, User::current());
                }

                TaskEvent::chatMessage()->trigger($task, User::current());

                $chatMembers = array_values(ArrayHelper::map(TeamMember::find()->where(['task_id' => $task->id])->andWhere(['!=', 'user_id', $user->id])->all(), 'task_id', 'user_id'));
                $users = ArrayHelper::map(User::findAll($chatMembers), 'id', 'id');

                Yii::$app->pusher
                    ->addReceivers($users)
                    ->setAdditionalData(['task_id' => $task->id])
                    ->compose([
                        'title' => "Пожалуйста, доработайте запрос №{$task->id}",
                        'body' => 'Комментарий:  '.$sendMessageForm->text,
                        'icon' => '',
                        'url' => Url::toRoute(['index', 'taskId' => $task->id], 'https'),
                    ])->send();
            }
        }

        return Chat::widget(['task' => $task]);
    }

    public function actionTestPush()
    {
        $result = Yii::$app->pusher
            ->addReceivers(['9' => '9'])
            ->compose([
                'title' => "Заголовок",
                'body' => 'Тело',
                'icon' => '',
//                'click_action' => Url::toRoute(['index', 'id' => $task->id]),
            ])->send();


        var_dump($result);
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex($taskId)
    {

        /**
         * @var Task $task
         */
        $this->task = Task::find()
            ->where(['id' => $taskId])
            ->limit(1)
            ->one();
        //mark as seen task
        $this->seenTask();

        return $this->render('index', [
            'task' => $this->task,
            'filesForm' => new AddAttachmentsForm(['taskId' => $this->task->id]),
        ]);
    }

    public function actionNewMessages($taskId)
    {
        if (!\Yii::$app->request->isPost) {
            return;
        }

        $lastMessage = \Yii::$app->request->post('lastMessage');
        /**
         * @var Task $task
         */
        $this->task = Task::find()
            ->where(['id' => $taskId])
            ->limit(1)
            ->one();

        $new = $this->task->getMessages()
            ->andWhere(['>', 'created_at', $lastMessage]
            )->count();

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['new' => $new];
    }
}

