<?php

namespace app\controllers;

use app\models\behaviors\UpdateOnlineBehavior;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\models\task\TaskSearch;
use app\models\team\roles\ObserverTeamRole;
use app\models\team\TeamMember;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\controllers\BasePermissionController;
use app\Rf\Modules\Users\models\User;
use app\Rf\Modules\Users\models\UserAction;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class CompletedTaskController extends TaskController
{
    public function userActions()
    {
        return [
            'index' => UserAction::TASK_VIEW,
            'view' => UserAction::TASK_VIEW,
            'update' => UserAction::TASK_UPDATE,
            'create' => UserAction::TASK_CREATE,
            'delete' => UserAction::TASK_DELETE,
            'bulk-delete' => UserAction::TASK_DELETE,
        ];
    }


    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $dataProvider->query->andFilterWhere(['status_id' => '10']);

        return $this->render('@app/views/task/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
