<?php

namespace app\controllers;

use app\models\behaviors\UpdateOnlineBehavior;
use app\models\faqPost\FaqPost;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use app\models\faqCategory\FaqCategory;
use yii\helpers\Html;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class ManualController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'updateOnline' => UpdateOnlineBehavior::class,
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays fAQ page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $categories = FaqCategory::find()->all();

        return $this->render('index', [
            'categories' => $categories,
        ]);
    }

    public function actionCreateCategory()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = new FaqCategory();


        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload' => '#faq-pjax-container',
                'title' => 'Добавить категорию',
                'content' => '<p class="text text-success">Категория создана</p>',
                'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Готово', ['class' => 'btn btn-primary', 'data-dismiss' => "modal"]),
            ];
        } else {
            return [
                'title' => 'Добавить категорию',
                'content' => $this->renderAjax('create-category', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "Save"), ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    public function actionCreatePost()
    {
        $request = Yii::$app->request;
        $model = new FaqPost();


        if($model->load($request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        } else {
            return $this->render('create-post', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdatePost($id)
    {
        $request = Yii::$app->request;
        $model = FaqPost::findOne($id);


        if($model->load($request->post()) && $model->save())
        {
            return $this->redirect(['index']);
        } else {
            return $this->render('create-post', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdateCategory($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = FaqCategory::findOne($id);


        if($model->load($request->post()) && $model->save())
        {
            return [
                'forceReload' => '#faq-pjax-container',
                'title' => 'Редактировать категорию',
                'content' => '<p class="text text-success">Категория изменена</p>',
                'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button('Готово', ['class' => 'btn btn-primary', 'data-dismiss' => "modal"]),
            ];
        } else {
            return [
                'title' => 'Редактировать категорию',
                'content' => $this->renderAjax('create-category', [
                    'model' => $model,
                ]),
                'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::button(Yii::t('app', "Save"), ['class' => 'btn btn-primary', 'type' => "submit"])

            ];
        }
    }

    /**
     * Delete an existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteCategory($id)
    {
        $request = Yii::$app->request;
        $model = FaqCategory::findOne($id);
        $model->delete();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose' => true, 'forceReload' => '#faq-pjax-container'];
    }

    /**
     * Delete an existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletePost($id)
    {
        $request = Yii::$app->request;
        $model = FaqPost::findOne($id);
        $model->delete();

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['forceClose' => true, 'forceReload' => '#faq-pjax-container'];
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['users/auth/login']);
        }

        if($action->id != 'index' && \app\Rf\Modules\Users\models\User::current()->isAdmin == false)
        {
            throw new ForbiddenHttpException('Доступ ограничен');
        }

        return parent::beforeAction($action);
    }
}
