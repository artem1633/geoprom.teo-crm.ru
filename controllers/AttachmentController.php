<?php

namespace app\controllers;

use app\models\AddFilesForm;
use app\models\attachment\AddAttachmentsForm;
use app\models\attachment\Attachment;
use app\models\behaviors\UpdateOnlineBehavior;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\Rf\Modules\Users\models\User;
use app\widgets\attachment\FilesWidget;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class AttachmentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'updateOnline' => UpdateOnlineBehavior::class,
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'add' => ['post'],
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionDelete($attachmentId)
    {
        $attachment = Attachment::findOne($attachmentId);
        if ($attachment) {
            $attachment->delete();
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionAdd()
    {
        $attachments = new AddAttachmentsForm();
        if ($attachments->load(\Yii::$app->request->post())) {

            $task = Task::findOne(['id' => $attachments->taskId]);
            $attachments->files = UploadedFile::getInstances($attachments, 'files');
            $attachments->save();

            foreach ($attachments->added as $attachment) {

                TaskEvent::attachmentAdd()
                    ->trigger($task, User::current(), ['attachment' => $attachment]);

            }
            return FilesWidget::widget([
                'task' => $task,
                'filesForm' => $attachments,
            ]);

        } else {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => false];
        }
    }
}

