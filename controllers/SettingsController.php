<?php

namespace app\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\settings\Settings;

/**
 * Settings Controller
 */
class SettingsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'change-setting-value-ajax' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @throws NotFoundHttpException
     * @throws BadRequestHttpException
     * @return mixed
     */
    public function actionChangeSettingValueAjax()
    {
        if(isset($_POST['key']) == false || isset($_POST['value']) == false)
        {
            throw new BadRequestHttpException('Отсутствуют обязательные параметры: key, value');
        } else {
            $key = $_POST['key'];
            $value = $_POST['value'];
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        $setting = Settings::findByKey($key);

        if($setting == null)
        {
            throw new NotFoundHttpException('Настройка не найдена');
        }

        $setting->value = $value;
        $result = $setting->save(false);

        return ['result' => $result];
    }

    /**
     * Список параметров
     * @return mixed
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;

        if($request->isPost)
        {
            $data = $request->post();
            foreach ($data['Settings'] as $key => $value){
                $setting = Settings::findByKey($key);

                if($setting != null){
                    $setting->value = $value;
                    $setting->save();
                    Yii::$app->session->setFlash('success', 'Настройки успешно сохранены');
                }
            }
        }

        $settings = Settings::find()->all();

        return $this->render('index', [
            'settings' => $settings,
        ]);
    }

}

