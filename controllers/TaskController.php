<?php

namespace app\controllers;

use app\models\behaviors\UpdateOnlineBehavior;
use app\models\task\Task;
use app\models\task\TaskEvent;
use app\models\task\TaskSearch;
use app\models\team\roles\ObserverTeamRole;
use app\models\team\TeamMember;
use app\models\team\TeamRole;
use app\Rf\Modules\Users\controllers\BasePermissionController;
use app\Rf\Modules\Users\models\User;
use app\Rf\Modules\Users\models\UserAction;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends BasePermissionController
{
    public function userActions()
    {
        return [
            'index' => UserAction::TASK_VIEW,
            'view' => UserAction::TASK_VIEW,
            'update' => UserAction::TASK_UPDATE,
            'create' => UserAction::TASK_CREATE,
            'delete' => UserAction::TASK_DELETE,
            'bulk-delete' => UserAction::TASK_DELETE,
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'updateOnline' => UpdateOnlineBehavior::class,
            'access' => [
                'class' => AccessControl::class,
                'only' => ['view', 'index', 'update', 'create', 'delete', 'bulk-delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['view', 'index', 'update', 'create', 'delete', 'bulk-delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere(['!=', 'status_id', '10']);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => Yii::t('app', 'Task') . " " . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' =>
                    Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    (User::current()->isAdmin ?
                        Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote']) :
                        null),
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Task model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Task();


        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                $executor = new TeamMember(['role' => TeamRole::executor()]);
                return [
                    'title' => Yii::t('app', "Create new task"),
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'executor' => $executor,
                    ]),
                    'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', "Save"), ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {

                $model->load($request->post());
                $model->status_id=1;
                $executor = new TeamMember(['role' => TeamRole::executor()]);
                $executor->load($request->post());


                if ($this->createTask($model, $executor)) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => Yii::t('app', "Create new task"),
                        'content' => '<span class="text-success">Запрос успешно создан</span>',
                        'footer' => Html::button(Yii::t('app', 'Close'), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a(Yii::t('app', "Создать ещё"), ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])

                    ];
                } else {
                    return [
                        'title' => Yii::t('app', "Create new task"),
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'executor' => $executor,
                        ]),
                        'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * @param Task $task
     * @param TeamMember $executor
     */
    protected function createTask(&$task, &$executor)
    {
        if ($task->validate()) {
            try {
                \Yii::$app->db->transaction(function ($db) use (&$task, &$executor) {
                    $task->save();
                    $executor->task = $task;

                    if (!$executor->user) {
                        $executor->addError('user_id', Yii::t('app', 'Invalid executor'));
                        throw new \Exception('Invalid executor');
                    }

                    if ($executor->validate()) {
                        $executor->save();

                        if($task->observersList != null)
                        {
                            $observersIds = $task->observersList;
                            foreach ($observersIds as $id)
                            {
                                $teamMember = TeamMember::find()->where(['task_id' => $task->id, 'user_id' => $id])->one();
                                if($teamMember == null){
                                    $teamMember = new TeamMember([
                                        'task_id' => $task->id,
                                        'user_id' => $id,
                                        'team_role_id' => ObserverTeamRole::FOREIGN,
                                    ]);
                                    $teamMember->save(false);
                                }
                            }
                        }
                    } else {
                        throw new \Exception('Invalid executor');
                    };
                });
            } catch (\Throwable $exception) {
                return false;
            }

            TaskEvent::createTask()->trigger($task, $task->author);
            return true;
        }
    }

    /**
     * Updates an existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => Yii::t('app', 'Update task') . " " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => Yii::t('app', 'Task') . " " . $id,
                    'content' => $this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::a(Yii::t('app', 'Edit'), ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                ];
            } else {
                return [
                    'title' => Yii::t('app', 'Update task') . " " . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(Yii::t('app', "Close"), ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button(Yii::t('app', 'Save'), ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Task model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }
}
