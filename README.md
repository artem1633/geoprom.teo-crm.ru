# Install

## Db setup.
1. Create db schema.
2. Copy example config  
`cp config/db.php.example config/db.php`
3. Setup db config  
`nano config/db.php` 

## Install dependencies
`composer install`

