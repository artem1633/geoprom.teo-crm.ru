<?php

namespace tests\models\task;

use app\models\task\Task;
use app\models\task\TaskStatus;
use app\models\team\TeamMember;
use app\Rf\Modules\Users\models\User;

class TaskTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testStatus()
    {
        $task = new Task();
        $task->status = TaskStatus::wait();
        expect($task->status)->equals(TaskStatus::wait());
        expect($task->status_id)->equals(TaskStatus::wait()->id);
    }

    const TEST_TASK_TITLE = 'TEST_TASK_TITLE';

    public function testCreateAndSave()
    {
        /**
         * @var Task $task
         */
        $task = new Task([
            'title' => self::TEST_TASK_TITLE,
            'body' => 'Текст задачи (тело)',
        ]);
        $task->currentUser = User::findOne(1);
        $task->status = TaskStatus::wait();

        expect_that($task->save());

        $savedTask = Task::findOne($task->id);
        expect($savedTask)->isInstanceOf(Task::class);
        expect($savedTask->author)->isInstanceOf(User::class);
        expect($savedTask->author->id)->greaterThan(0);
    }

    public function testHasAuthor()
    {
        $task = Task::findOne(['title' => self::TEST_TASK_TITLE]);
        $author = $task->author;
        expect($author)->isInstanceOf(User::class);
    }

    public function testDelete()
    {
        $task = Task::findOne(['title' => self::TEST_TASK_TITLE]);
        $id = $task->id;
        expect_that($task->delete());
        expect(TeamMember::find()->where(['task_id' => $id])->count())->equals(0);
    }




}
