<?php

namespace tests\models\task;

use app\models\notification\Notification;
use app\models\task\Task;
use app\models\task\TaskEvent;

class TaskEventTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testCreateEvent()
    {
        /**
         * @var Task $task
         */
        $task = Task::find()->limit(1)->one();

        TaskEvent::createTask()->trigger($task, $task->author);

        /**
         * @var Notification $n
         */
        $n = Notification::find()
            ->where([
                    'task_id' => $task->id,
                    'user_id' => $task->executor->id,
                    'event_id' => TaskEvent::createTask()->id,
                ]
            )->limit(1)
            ->one();

        expect_that($n);
        expect_that($n->important);
        expect($n->event)->equals(TaskEvent::createTask());
        expect($n->message)->contains('Новый запрос');

        $n->delete();
    }

    public function testChangeStatusEvent()
    {
        /**
         * @var Task $task
         */
        $task = Task::find()->limit(1)->one();

        TaskEvent::changeStatus()->trigger($task, $task->author);

        /**
         * @var Notification $n
         */
        $n = Notification::find()
            ->where([
                    'task_id' => $task->id,
                    'user_id' => $task->executor->id,
                    'event_id' => TaskEvent::changeStatus()->id,
                ]
            )->limit(1)
            ->one();

        expect_that($n);
        expect_that($n->important);
        expect($n->event)->equals(TaskEvent::changeStatus());
        expect($n->message)->contains('Изменен статус запроса');

        $n->delete();
    }


}
