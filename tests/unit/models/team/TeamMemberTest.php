<?php

namespace tests\models\team;

use app\models\task\Task;
use app\models\team\TeamMember;
use app\Rf\Modules\Users\models\User;

class TeamMemberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testCreate()
    {
        $task = Task::findOne(2);
        $user = User::findOne(1);
        $member = TeamMember::author($user, $task);
        expect($member)->isInstanceOf(TeamMember::class);
        expect($member->user_id)->equals($user->id);
        expect($member->task_id)->equals($task->id);
        var_dump($member);
    }


}
