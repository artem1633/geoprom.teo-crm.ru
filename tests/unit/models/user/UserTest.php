<?php

namespace tests\models\user;

use app\models\user\Online;
use app\Rf\Modules\Users\models\User;

class TeamMemberTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testOnline()
    {
        /**
         * @var User $user
         */
        $user = User::find()->where(['id' => 2])->limit(1)->one();
        $online = $user->online;
        expect($online)->isInstanceOf(Online::class);

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($online->seen, new \DateTimeZone(\Yii::$app->timeZone));

        //var_dump($nowDt->getTimestamp());
        //var_dump($seen->getTimestamp());
        //var_dump($nowDt->getTimestamp() - $seen->getTimestamp());

        var_dump($user->isOnline);

        //var_dump($now - strtotime($online->seen));

    }

    public function testGetOnline()
    {
        /**
         * @var User $user
         */
        $user = User::find()->where(['id' => 2])->limit(1)->one();
        $online = $user->online;
        var_dump($online);
        expect($online)->isInstanceOf(Online::class);


    }

}
