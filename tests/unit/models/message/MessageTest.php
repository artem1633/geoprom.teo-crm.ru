<?php

namespace tests\models\message;

use app\models\message\Message;
use app\models\message\MessageType;
use app\models\task\Task;
use app\Rf\Modules\Users\models\User;

class MessageTest extends \Codeception\Test\Unit
{
    const TEST_MESSAGE_TEXT = 'TEST_MESSAGE_TEXT';
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testCreateAndSave()
    {
        /**
         * @var Message $message
         */
        $message = Message::create(
            Task::find()->limit(1)->one(),
            User::find()->limit(1)->one(),
            MessageType::service()
        )->withText(self::TEST_MESSAGE_TEXT);


        expect_that($message->save());

        $savedMessage = Message::findOne($message->id);
        expect($savedMessage->user)->isInstanceOf(User::class);
        expect($savedMessage->task)->isInstanceOf(Task::class);
        expect($savedMessage->text)->equals(self::TEST_MESSAGE_TEXT);
    }

    public function testDelete()
    {
        $message = Message::findOne(['text' => self::TEST_MESSAGE_TEXT]);
        $id = $message->id;
        expect_that($message->delete());
    }


}
