var app = require('express')();
var https = require('https');
var fs = require('fs');

var privateKey = fs.readFileSync('key.pem');
// var privateKey = fs.readFileSync('ryans-key.pem');
var certificate = fs.readFileSync('cert.pem');
// var certificate = fs.readFileSync('ryans-cert.pem');
// var ca = fs.readFileSync('ryans-csr.pem');

var server = https.createServer({
    // ca: ca,
    key: privateKey,
    cert: certificate,
},app);


server.listen(3000);


app.get("/send-push", function(request, response){

    response.setHeader('Content-Type', 'application/json');

    console.log('push request');
    console.log(request.query);

    var title = request.query.title;
    var body = request.query.body;
    var url = request.query.url;
    var uid = request.query.uid.toString();
    var task_id = request.query.task_id;

    console.log('pushing');

    if(clients[uid] != undefined)
    {
        clients[uid].emit('message', {'title': title, 'data': {'body': body, 'url': url, 'task_id': task_id}});
        response.send(JSON.stringify({'result': true, 'message': 'Success emited'}));
    } else {
        response.send(JSON.stringify({'result': false, 'message': 'Uid not found'}));
    }
})

var io = require('socket.io').listen(server);

var clients = [];

io.sockets.on('connection',function (socket) {
    console.log('New connection');
    var handshakeData = socket.request;

    socket.on('notify', function (data) {
        console.log(data);
        var uid = data.uid;
        clients[uid].emit('message', data);
        io.emit('user disconnected');
        // socket.broadcast.emit('message', data);
    });

    if(handshakeData._query['uid'] != undefined)
    {
        var uid = handshakeData._query['uid'];
        console.log("Connected user id: ", uid);

        uid = uid.toString();

        clients[uid] = socket;
        // console.log(clients);
    } else {

    }

    socket.on('disconnect', function () {
        io.emit('user disconnected');
    });
});
