<?php

use yii\helpers\Html;

?>
    <p>
        1. Нажмите на знак "+" в списке запросов
        <?= Html::img('/manual/tasks/new_task.png', ['class' => 'img-thumbnail']); ?>
    </p>

    <p>
        2. В модальном окне заполните поля данных запроса
        <?= Html::img('/manual/tasks/new_task_modal.png', ['class' => 'img-thumbnail']); ?>
    </p>
    <p class="text-danger">Важно! После сохранения их изменить может только администратор</p>

    <p>3. Новый запрос появится в списке запросов</p>
<?= Html::img('/manual/tasks/task_row.png', ['class' => 'img-thumbnail']); ?>