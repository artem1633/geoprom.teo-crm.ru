<?php

use yii\helpers\Html;

?>
<p>Главное меню в виде сворачивающейся боковой панели:</p>
<?= Html::img('/manual/menu/main_menu_items.png', ['class' => 'img-thumbnail']); ?>

<p>Элементы:</p>
<ul>
    <li>
        <p><?= Yii::t('app', 'Users') ?></p>
        <p class="text text-muted">Управление пользователями</p>
        <p class="text-danger">Доступно только администраторам</p>
    </li>
    <li>
        <p><?= Yii::t('app', 'Tasks') ?></p>
        <p class="text text-muted">Список запросов</p>
    </li>
    <li>
        <p><?= Yii::t('app', 'Notifications') ?></p>
        <p class="text text-muted">Список уведомлений для пользователя</p>
    </li>
    <li>
        <p><?= Yii::t('app', 'Manual') ?></p>
        <p class="text text-muted">Инструкция пользователя</p>
    </li>
</ul>