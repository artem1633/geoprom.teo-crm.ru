<?php

namespace app\components;

use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

class Pusher extends Component
{
    /**
     * @var String Api ключ от Firebase Cloud messages service
     */
    public $apiKey;

    /**
     * @var String Url для отправки пушей
     */
    public $pushUrl = 'https://geoprom.teo-crm.ru:3000/send-push/?';

    /**
     * @var array Получатели
     */
    protected $receivers = [];

    /**
     * @var array Дополнительная информация
     */
    public $additionalData = [];

    /**
     * @var array Тело сообщения
     */
    protected $message;


    public function init()
    {
        if($this->apiKey === null)
            throw new InvalidConfigException('option $apiKey must be not empty');

        parent::init();
    }

    /**
     *	Добавляет токен или токены, владельцам которых будут отправлены уведомления
     *  @param String|array $receivers
     *	@return \app\components\Pusher $this
     */
    public function addReceivers($receivers)
    {
        if(is_array($receivers)){
            $this->receivers = array_merge($this->receivers, $receivers);
        } else {
            $this->receivers[] = $receivers;
        }
//
//        echo $receivers[0];
//
        return $this;
    }

    /**
     * Присоединяет доплнительную информацию к сообщению
     * @param $data
     * @return $this
     */
    public function setAdditionalData($data)
    {
        $this->additionalData = $data;
        return $this;
    }

    /**
     * Устанавливает тело сообщения
     * @param array $message Тело сообщения
     * - `title`: _string_, Заголовок пуша
     * - `body`: _string_, Сообщение пуша
     * - `icon`: _string_, Ссылка на изображение пуша
     * - `click_action`: _string_, Ссылка, по которой пользователь переходит
     * при нажатии на пуш
     * @return \app\components\Pusher $this
     */
    public function compose($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * Отправляет сообщения, возвращая ответ от сервера Firebase
     * @return array
     */
    public function send()
    {
        $result = [];


        foreach($this->receivers as $id)
        {
            $request_body = [
                'to' => $token,
                'notification' => $this->message,
            ];
            $fields = json_encode($request_body);

            $request_headers = [
                'Content-Type: application/json',
                'Authorization: key=' . $this->apiKey,
            ];

            try{
                $this->message['uid'] = $id;
                if(!empty($this->additionalData)){
                    $this->message = ArrayHelper::merge($this->message, $this->additionalData);
                }
                $query = http_build_query($this->message);

                $url = $this->pushUrl.$query;

                // echo '<br>'.$url.'<br>';



                // file_get_contents($url);


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                // curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
                // curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                $result[] = $response;


                // DEBUG ::
                // $res = curl_getinfo($ch);
//                var_dump($res);

                // var_dump($res);

                // curl_close($ch);
            } catch (\Exception $e) {
                // TODO: EXCEPTION HANDLING
            }
        }

        return $result;
    }

}

?>