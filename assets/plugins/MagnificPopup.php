<?php

namespace app\assets\plugins;

use yii\web\AssetBundle;

/**
 * Class MagnificPopup
 * @package app\assets\plugins
 */
class MagnificPopup extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'plugins/magnificPopup/magnificpopup.css',
    ];
    public $js = [
        'plugins/magnificPopup/magnificpopup.min.js',
    ];

    public $depends = [
        'app\assets\AppAsset',
    ];
}