<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $js = [
        'js/project.js',

        // PUSHING
//        'firebase.js',
        'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.2.0/socket.io.js',
//        'https://www.gstatic.com/firebasejs/5.8.0/firebase-app.js',
//        'https://www.gstatic.com/firebasejs/5.8.0/firebase-auth.js',
//        'https://www.gstatic.com/firebasejs/5.8.0/firebase-database.js',
//        'https://www.gstatic.com/firebasejs/5.8.0/firebase-firestore.js',
//        'https://www.gstatic.com/firebasejs/5.8.0/firebase-messaging.js',
        'js/app.js',
        'push_init.js',
//        'firebase_subscribe.js',
    ];
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
