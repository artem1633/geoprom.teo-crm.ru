function changeFavicon(path)
{
    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = path;
    document.getElementsByTagName('head')[0].appendChild(link);
}

var firstChecked = null;
var secondChecked = null;

$('table input[name="selection[]"]').each(function(i){
    $(this).parent().parent().attr('data-count', i);
    console.log('each');
});


$('table input[name="selection[]"]').click(function(event){
    if($(this).is(':checked')){
        var count = $(this).parent().parent().data('count');
        if(firstChecked == null){
            firstChecked = count;
            secondChecked = null;
        } else {
            if(event.shiftKey){
                secondChecked = count;
                if(secondChecked < firstChecked){
                    secondChecked = firstChecked;
                    firstChecked = count;
                }
                $('table input[name="selection[]"]').each(function(i){
                    var number = parseInt($(this).parent().parent().attr('data-count'));
                    if(number > firstChecked && number < secondChecked){
                        $(this).attr('checked', 'checked');
                    }
                });
                firstChecked = null;
                secondChecked = null;
            }
        }
    } else {
        console.log('not checked');
    }
});