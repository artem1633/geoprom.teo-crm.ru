// Initialize Firebase
var config = {
    apiKey: "AIzaSyDKexG_LUQCuIDXZHT5erJsmMce_xLDtns",
    authDomain: "geoprom-9c8f2.firebaseapp.com",
    databaseURL: "https://geoprom-9c8f2.firebaseio.com",
    projectId: "geoprom-9c8f2",
    storageBucket: "geoprom-9c8f2.appspot.com",
    messagingSenderId: "109591204625"
};
firebase.initializeApp(config);

  var messaging = firebase.messaging();

  messaging.requestPermission()
  	.then(function() {
  		console.log('Have permission');
  		return messaging.getToken();
  	})
  	.then(function(token){
  		console.log(token);

      if(token){
        console.log('okey');
        sendTokenToServer(token);
      } else {
        setTokenSentToServer(false);
      }
  	})
  	.catch(function(error){
  		console.log('Error Ocured');
  		console.log(error);
  	});

 messaging.onMessage(function(payload){
 	console.log(payload);
 });


 function updatePushToken()
 {
     window.localStorage.removeItem('sentFirebaseMessagingToken');
     var messaging = firebase.messaging();


     messaging.requestPermission()
         .then(function() {
             console.log('Have permission');
             return messaging.getToken();
         })
         .then(function(token){
             console.log(token);

             console.log('token: '+token);

             if(token){
                 console.log('okey');
                 sendTokenToServer(token);
             } else {
                 setTokenSentToServer(false);
             }
         })
         .catch(function(error){
             console.log('Error Ocured');
             console.log(error);
         });

     messaging.onMessage(function(payload){
         console.log(payload);
     });
 }

 function sendTokenToServer(currentToken){
    if(!isTokenSentToServer(currentToken)) {
      console.log('Регистрация токена');

      var url = window.location.origin+'/index.php?r=site%2Fsave-token';
      var csrfToken = $('meta[name="csrf-token"]').attr('content');

      $.post(url, {
          '_csrf': csrfToken,
          'token': currentToken
      });

      setTokenSentToServer(currentToken);
    } else {
      console.log('Токен уже зарегистрирован');
    } 
 }

 // используем localStorage для отметки того,
 // что пользователь уже подписался на уведомления
 function isTokenSentToServer(currrentToken){
    return window.localStorage.getItem('sentFirebaseMessagingToken') == currrentToken;
 }

 function setTokenSentToServer(currrentToken){
     $('#push-btn').hide();
    window.localStorage.setItem(
        'sentFirebaseMessagingToken',
        currrentToken ? currrentToken : ''
      );
 }


