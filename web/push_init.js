var uid = $('meta[name="uid"]').attr('content');

console.log('Current user id: '+uid);

var socket = io('https://geoprom.teo-crm.ru:3000', {secure: true, query: 'uid='+uid});


if(Notification.permission != 'granted'){
    Notification.requestPermission();
}

socket.on('message', function(msg){
    console.log('Message, ', msg);

    var faviconNow = 'favicon.ico';

    setInterval(function () {

        if(faviconNow == 'favicon.ico')
        {
            faviconNow = 'favicon_message.ico';
        } else if(faviconNow = 'favicon_message.ico') {
            faviconNow = 'favicon.ico';
        }

        $('[data-task-row-id="'+msg.data.task_id+'"]').removeClass('my_last_message').addClass('member_unread_warning');

        changeFavicon(faviconNow);
    },500);

    var notification = new Notification(msg.title, msg.data);
    notification.onclick = function(event) { window.open(msg.data.url); notification.close(); };
    setTimeout(notification.close.bind(notification), 7200000);
});

