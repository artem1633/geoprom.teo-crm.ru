var push = PushNotification.init({
    "android": {
        "senderID" : "21568......",
        "forceShow" : "true",
        "vibrate" : "true",
        "sound" : "true"
    },
    browser: {
    },
    ios: {
        alert: "true",
        badge: "true",
        sound: "true"
    },
    windows: {}
});

var enabledAPI,regAPI = 0 ;
pushEnabled($q).then(function(status) {
    enabledAPI = status ;  // push enabled or not
    return pushReg($q) ;
}).then(function(status) {
    regAPI = status ;  // pushID changed or not
    if (enabledAPI == 1 || regAPI == 1) {
        apiService.all(... ... ) ;
    }
}) ;

function pushEnabled($q) {
    var q = $q.defer()
    PushNotification.hasPermission(function(data) {
        var oldPushEnabled = getDB('dev_pushEnabled') ;
        if (data.isEnabled == true) {
            var pushEnabled = 1 ;
        } else {
            var pushEnabled = 0 ;
        }
        if (oldPushEnabled != pushEnabled) {
            setDB('dev_pushEnabled',pushEnabled,1) ;
            q.resolve(1) ;  // push enable status has changed
        } else {
            q.resolve(0) ;  // push enable status has not changed
        }
    });
    return q.promise ;
}

function pushReg($q) {
    var q = $q.defer() ;
    push.on('registration', function(data) {
        var oldRegId = getDB('dev_pushID');
        if (oldRegId != data.registrationId) {
            // Save new registration ID
            setDB('dev_pushID', data.registrationId,1);
            // Post registrationId to your app server as the value has changed
            q.resolve(1) ;  // pushID has changed
            console.log("IDs have CHANGED!") ;
        } else {
            console.log("IDs the same") ;
            q.resolve(0) ;  // pushID has not changed.
        }
    });
    return q.promise
}