// firebase-messaging-sw.js
importScripts('https://www.gstatic.com/firebasejs/5.8.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.8.0/firebase-messaging.js');

firebase.initializeApp({
    messagingSenderId: '852849284917'
});

const messaging = firebase.messaging();