<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'name' => 'GeoProm',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'defaultRoute' => 'site/index',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@uploads' => 'uploads',
    ],
    'components' => [
        'pusher' => [
            'class' => 'app\components\Pusher',
            'apiKey' => 'AIzaSyDKexG_LUQCuIDXZHT5erJsmMce_xLDtns',
        ],
        'formatter' => [
            'dateFormat' => 'dd.MM.yyyy',
            //'dateFormat' => 'yyyy-MM-dd',
            'timeZone' => 'UTC',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '337RbIdkrE8JOw_Idf9DASLd9sd3ld021md',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => \app\Rf\Modules\Users\models\User::class,
            'enableAutoLogin' => true,
            'loginUrl' => ['users/auth/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
//                MAIL.RU
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'smtp.mail.ru',
//                'username' => 'geoprom.notifications@mail.ru',
//                'password' => 'Kl347;sDn3fg',
//                'port' => '465',
//                'encryption' => 'ssl',

            // YANDEX.RU
                  'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.com',
                'username' => 'geoprom.notifications@yandex.ru',
                'password' => 'Kl347;sDn3fg',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                'users*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/Rf/Modules/Users/messages',
                ],
            ],
        ],
        'urlManager' => [

            'enablePrettyUrl' => false,
            'showScriptName' => false,
            'rules' => [
                '/' => 'projects/index',
                '<controller:\w+>/<action:update|view|delete>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<controller:\w+>' => '<controller>/index',
            ],
        ],
        'db' => $db,

    ],
    'timeZone' => 'Europe/Moscow',
    'params' => $params,
    'modules' => [
        'users' => [
            'class' => \app\Rf\Modules\Users\Module::class,
            // ... другие настройки модуля ...
        ],

        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
    ],
];

if (YII_ENV_DEV ) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
//        'allowedIPs' => ['127.0.0.1', '::1'],
        'allowedIPs' => ['*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'adminlte' => '@vendor/dmstr/yii2-adminlte-asset/gii/templates/crud/simple',
                ]
            ]
        ],
    ];
}

return $config;
