<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\migrations', // Общие миграции приложения
            ],
            'migrationPath' => null
        ],
        'migrate-users' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'app\Rf\Modules\Users\migrations'
            ],
            'migrationTable' => 'rf_users_migration',
            'migrationPath' => null
        ],
    ],


    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'Country' => [
            'class' => 'app\components\Country',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'timeZone' => 'Europe/Moscow',
    'params' => $params,
    'modules' => [
        'users' => [
            'class' => \app\Rf\Modules\Users\Module::class,
            //[]
        ],
    ]
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
