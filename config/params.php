<?php

return [
    'onlineSeenLimit' => 2 * 60, //sec
    'adminEmail' => 'admin@example.com',
    'version' => '0.4.2',
    'bsVersion' => '3.x',
    'icon-framework' => \kartik\icons\Icon::FA,
];
